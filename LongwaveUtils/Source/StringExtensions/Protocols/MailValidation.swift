//
//  MailValidation.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 10/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public protocol MailValidation {
    /// Validate a string
    func isValidEmail() -> Bool
}


