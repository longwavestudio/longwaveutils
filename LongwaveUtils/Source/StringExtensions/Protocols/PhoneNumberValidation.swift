//
//  PhoneNumberValidation.swift
//  LongwaveUtils
//
//  Created by Massimo Cesaraccio on 05/05/2021.
//

import Foundation

public protocol PhoneNumberValidation {
    /// Validate a string
    func isValidPhoneNumber() -> Bool
}
