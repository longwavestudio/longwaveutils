//
//  String+MailValidation.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 10/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

extension String: MailValidation {
    /// Regex to validate email
    public func isValidEmail() -> Bool {
        // LogDebug("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

