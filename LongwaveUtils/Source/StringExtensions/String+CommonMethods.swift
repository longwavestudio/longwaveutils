//
//  String+CommonMethods.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//
import Foundation

extension String {
    /// Returns a string representing playback time.
    ///
    /// - Parameters:
    ///   - time: the time to be converted.
    ///   - alwaysFull: if _true_ will always use the full HH:MM:SS even if hours is zero
    /// - Returns: a stirng representing the time.
    static func stringFromPlaybackTime(_ time: TimeInterval, alwaysFull: Bool = false) -> String {
        let (hours, minutes, seconds) = convertPlaybackTime(time)
        guard alwaysFull || hours > 0 else {
            return String(format: "%02lld:%02lld", minutes, seconds)
        }
        let format = alwaysFull ? format_HH_MM_SS() : format_H_MM_SS()
        return String(format: format, hours, minutes, seconds)
    }

    fileprivate static func format_HH_MM_SS() -> String {
        return "%02lld:%02lld:%02lld"
    }

    fileprivate static func format_H_MM_SS() -> String {
        return "%lld:%02lld:%02lld"
    }
    
    fileprivate static func convertPlaybackTime(_ time: TimeInterval) -> (hours: Int64, minutes: Int64, seconds: Int64) {
        let h = Int64(time / 3600)
        let m = (Int64(time) / 60) % 60
        let s = Int64(time) % 60
        return (hours: h, minutes: m, seconds: s)
    }
}
