//
//  String+PhoneNumberValidation.swift
//  LongwaveUtils
//
//  Created by Massimo Cesaraccio on 05/05/2021.
//

import Foundation

extension String: PhoneNumberValidation {
    /// Regex to validate email
    public func isValidPhoneNumber() -> Bool {
        guard count > 9 else { return false }
        let regEx = "[+]?[0-9]+"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: self)
    }
}

