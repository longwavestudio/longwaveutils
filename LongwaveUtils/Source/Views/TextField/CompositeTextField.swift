//
//  CompositeTextField.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 28/05/2020.
//

import UIKit

public enum TextFieldType {
    case text
    case secureText
    case readOnlyText
    case readOnlyTextWithInput
}

public enum TextFieldState {
    case contentNotChecked
    case contentOk
    case contentNotOk(error: PrintableError)
}

public struct LabelTheme {
    var labelFont: UIFont
    var labelColor: UIColor
}

public struct TextFieldTheme {
    var textFont: UIFont
    var textColor: UIColor
    var textFieldBorderColor: UIColor
    var textFieldErrorBorderColor: UIColor
    var placeholderFont: UIFont
    var placeholderColor: UIColor
    var deleteImage: UIImage = UIImage()
    var checkOkImage: UIImage = UIImage()
    var checkKoImage: UIImage = UIImage()
    var secureInvisibleImage: UIImage = #imageLiteral(resourceName: "Occhio_OFF")
    var secureVisibleImage: UIImage = #imageLiteral(resourceName: "Occhio_ON")
}

public struct CompositeTextFieldTheme {
    var titleLabelTheme: LabelTheme
    var textField: TextFieldTheme
    var errorLabelTheme: LabelTheme
    
    static func defaultTheme() -> CompositeTextFieldTheme {
        let titleLabelTheme = LabelTheme(labelFont: UIFont.boldSystemFont(ofSize: 15),
                                         labelColor: UIColor.darkGray)
        let textFieldTheme = TextFieldTheme(textFont: UIFont.boldSystemFont(ofSize: 16),
                                            textColor: UIColor.darkGray,
                                            textFieldBorderColor: UIColor.green,
                                            textFieldErrorBorderColor: UIColor.red,
                                            placeholderFont: UIFont.systemFont(ofSize: 16),
                                            placeholderColor: UIColor.gray,
                                            deleteImage: UIImage(),
                                            checkOkImage: UIImage(),
                                            checkKoImage: UIImage(),
                                            secureInvisibleImage: UIImage(),
                                            secureVisibleImage: UIImage())
        let errorLabelTheme = LabelTheme(labelFont: UIFont.systemFont(ofSize: 15),
                                         labelColor: UIColor.red)
        let theme = CompositeTextFieldTheme(titleLabelTheme: titleLabelTheme,
                                            textField: textFieldTheme,
                                            errorLabelTheme: errorLabelTheme)
        return theme
    }
}

final public class CompositeTextField: UIView {
    private static var xibName = "CompositeTextField"
    
    public var onTextDidChange: ((_ newTest: String) -> Void)?
    public var onInputRequired: ((_ inputCompleted: @escaping (String) -> Void) -> Void)?

    public var type: TextFieldType = .text {
        didSet {
            switch type {
            case .secureText:
                isTextSecured = true
                currentTextField.rightView = rightView(for: "")
            case .readOnlyTextWithInput:
                addButton()
            case .text, .readOnlyText:
                break
            }
        }
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currentTextField: UITextField!
    @IBOutlet weak var textFieldContainer: UIView!

    @IBOutlet var errorLabel: UILabel!
    
    var status: TextFieldState = .contentOk {
        didSet {
            switch status {
            case .contentNotChecked:
                removeError()
            case .contentNotOk(let error):
                addError(error: error)
                currentTextField.rightView = rightView(for: "")
            case .contentOk:
                removeError()
                currentTextField.rightView = rightView(for: "")
            }
        }
    }
    private var isTextSecured: Bool = false {
        didSet {
            currentTextField.isSecureTextEntry = isTextSecured
            currentTextField.rightView = rightView(for: "")
        }
    }
    
    private var theme: CompositeTextFieldTheme = CompositeTextFieldTheme.defaultTheme()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        applyTheme()
        removeError()
    }
    
    public static func create(theme: CompositeTextFieldTheme) -> CompositeTextField {
        let bundle = Bundle(for: CompositeTextField.self)
        let textField = bundle.loadNibNamed(CompositeTextField.xibName,
                                            owner: nil,
                                            options: nil)!.first as! CompositeTextField
        textField.theme = theme
        textField.type = .text
        textField.status = .contentNotChecked
        textField.applyTheme()
        return textField
    }
    
    var onReadOnlyTextButtonDidTap: (() -> Void)?
    
    func updateState(_ state: TextFieldState) {
        self.status = state
    }
    
    private func addButton() {
        let button = UIButton()
        button.frame = textFieldContainer.frame
        button.addTarget(self, action: #selector(inputRequired), for: .touchUpInside)
        textFieldContainer.addSubview(button)
    }
    
    private func applyTheme() {
        applyTitleLabelTheme()
        applyTextFieldTheme()
        applyErrorLabelTheme()
    }
    
    private func applyTitleLabelTheme() {
        titleLabel.font = theme.titleLabelTheme.labelFont
        titleLabel.textColor = theme.titleLabelTheme.labelColor
    }
    
    private func applyTextFieldTheme() {
        currentTextField.font = theme.textField.textFont
        currentTextField.textColor = theme.textField.textColor
        let attributes = [
            NSAttributedString.Key.foregroundColor: theme.textField.placeholderColor,
            NSAttributedString.Key.font : theme.textField.placeholderFont
        ]
        currentTextField.attributedPlaceholder = NSAttributedString(string: "Placeholder Text", attributes:attributes)
    }
    
    private func applyErrorLabelTheme() {
        errorLabel.font = theme.errorLabelTheme.labelFont
        errorLabel.textColor = theme.errorLabelTheme.labelColor
    }
    
    private func removeError() {
        stackView.removeArrangedSubview(errorLabel)
        errorLabel.removeFromSuperview()
        textFieldContainer.layer.borderWidth = 1
        textFieldContainer.layer.borderColor = theme.textField.textFieldBorderColor.cgColor
    }
    
    private func addError(error: PrintableError) {
        stackView.addArrangedSubview(errorLabel)
        errorLabel.text = error.getErrorMessage()
        textFieldContainer.layer.borderWidth = 1
        textFieldContainer.layer.borderColor = theme.textField.textFieldErrorBorderColor.cgColor
    }
    
    private func rightView(for string: String) -> UIView? {
        switch status {
        case .contentOk:
            return UIImageView(image: theme.textField.checkOkImage)
        case .contentNotOk(_):
            return UIImageView(image: theme.textField.checkKoImage)
        case .contentNotChecked:
           return rightButton(for: string)
        }
    }
    
    private func rightButton(for string: String) -> UIButton? {
        let button = UIButton()
        button.frame = CGRect(x: 0,
                              y: 0,
                              width: 30,
                              height: 30)
        switch type {
        case .text, .readOnlyTextWithInput, .readOnlyText:
            if string.isEmpty {
                return nil
            } else {
                setupCancelButton(button: button)
            }
        case .secureText:
            setupSecureTextButton(button: button)
        }
        return button
    }
    
    private func setupCancelButton(button: UIButton) {
        button.setImage(theme.textField.deleteImage,
                        for: .normal)
        button.addTarget(self,
                         action: #selector(deleteText),
                         for: .touchUpInside)
    }
    
    private func setupSecureTextButton(button: UIButton) {
        if isTextSecured {
            button.setImage(theme.textField.secureVisibleImage,
                            for: .normal)
        } else {
            button.setImage(theme.textField.secureInvisibleImage,
                            for: .normal)
        }
        button.addTarget(self,
                         action: #selector(makeTextSecured),
                         for: .touchUpInside)
    }
    
    @objc func deleteText() {
        currentTextField.text = ""
        currentTextField.resignFirstResponder()
    }
    
    @objc func inputRequired() {
        onInputRequired?() { [weak self] string in
            self?.currentTextField.text = string
        }
    }
    
    @objc func makeTextSecured() {
        isTextSecured = !isTextSecured
    }
}

extension CompositeTextField: TextDidChangeObserver {}
extension CompositeTextField: UITextFieldDelegate {
    public func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        let text = textField.textFieldUpdatedText(changingCharactersIn: range,
                                                  replacementString: string)
        textField.rightView = rightView(for: text)
        return updatedTextField(textField,
                                shouldChangeCharactersIn: range,
                                replacementString: string)
    }
}
