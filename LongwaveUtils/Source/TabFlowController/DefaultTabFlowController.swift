//
//  DefaultTabFlowController.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 04/05/2020.
//

import UIKit

public final class DefaultTabFlowController {
    
    private var dependency: TabFlowDependency
    public weak var tabController: UITabBarController?
    
    private weak var currentEmbedder: UIViewController?
    private weak var menu: MenuFlow?
    
    public init(dependency: TabFlowDependency) {
        self.dependency = dependency
    }
}

extension DefaultTabFlowController: TabFlowController {
    
    public func embed(in viewController: UIViewController,
                      onCompletion: @escaping (_ info: [String: Any]) -> Void) {
        self.currentEmbedder = viewController
        let tab = UITabBarController()
        self.tabController = tab
        self.menu = dependency.menu
        viewController.embed(viewController: tab)
        embedContainers(in: tab,
                        menuFlowController: menu,
                        onCompletion: onCompletion)
    }
    
    public func disembed() {
        if let tab = tabController {
            currentEmbedder?.removeEmbeddedViewController(embeddedViewController: tab)
        } 
    }
    
    private func embedContainers(in tabBarController: UITabBarController,
                                 menuFlowController: MenuFlow?,
                                 onCompletion: @escaping (_ info: [String: Any]) -> Void) {
        var controllers: [UIViewController] = []
        for flow in dependency.flows {
            let controller = flow.buildRoot()
            flow.onActionRequested = onCompletion
            controllers.append(controller)
            subscribe(flow: flow, for: menuFlowController)
        }
        tabBarController.setViewControllers(controllers, animated: false)
        for (index, flow) in dependency.flows.enumerated() {
            tabBarController.tabBar.items?[index].image = flow.icon
            tabBarController.tabBar.items?[index].selectedImage = flow.iconSelected
            tabBarController.tabBar.items?[index].title = flow.title
            flow.onSetBadge = { [weak tabBarController, weak self] badge in
                guard let tabs = tabBarController, let self = self else { return }
                self.updateTab(atIndex: index, in: tabs, withBadge: badge)
            }
        }
    }
    
    private func updateTab(atIndex index: Int, in tabController: UITabBarController, withBadge badge: TabItemBadge) {
        let isSelected = tabController.selectedIndex == index
        switch (badge.updateCondition, isSelected) {
        case (.always, _), (.onlyWhenUnselected, false):
            tabController.tabBar.items?[index].badgeValue = badge.value
            tabController.tabBar.items?[index].badgeColor = badge.color
        default:
            break
        }
    }
    
    private func subscribe(flow: TabFlowEmbeddable, for menuFlowController: MenuFlow?) {
        guard let menuFlow = menuFlowController else {
            return
        }
        flow.subscribe { [weak menuFlow, weak self] type, callback in
            guard let self = self else { return }
            menuFlow?.setup(type: type,
                            actionBlock: { [weak self] (action, tab) in
                                self?.tabController?.selectedIndex = tab
                                print("should call action: \(action)")
                                callback(action)
            })
            if let tab = self.tabController {
                menuFlow?.embedMenu(in: tab,
                                    animated: true,
                                    type: type,
                                    onClose: { [weak self] in
                                        self?.menu = nil
                })
            }
        }
    }
}
