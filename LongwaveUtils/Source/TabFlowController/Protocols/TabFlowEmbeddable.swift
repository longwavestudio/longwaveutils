//
//  TabFlowEmbeddable.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 05/05/2020.
//

import UIKit

public typealias MenuEventBlock = (_ action: String, _ tab: Int) -> Void
public typealias MenuFlowEventBlock = (_ action: String) -> Void

public protocol TabFlowEmbeddable: AnyObject {
    var icon: UIImage { get }
    var iconSelected: UIImage { get }
    var title: String { get }
    /// Called by a tab flow to to request an action to be performed.
    var onActionRequested: ((_ info: [String: Any]) -> Void)? { get set }
    /// Called by a tab flow to update the corresponding tab bar item badge.
    var onSetBadge: ((TabItemBadge) -> Void)? { get set }
    func embed(in viewController: UIViewController)
    func buildRoot() -> UIViewController
    func subscribe(onMenuOpen: @escaping (_ menuType: String?,
                                          _ onMenuEvent: @escaping MenuFlowEventBlock) -> Void)
}

/// A descriptor for updates to a TabBarItem badge.
public struct TabItemBadge {
    /// The value to be displayed.
    var value: String?
    /// The color to be used when displayed.
    var color: UIColor?
    /// Defines wheter the badge should be updated if the tab is the currenlty selected one.
    var updateCondition: Condition
    
    public init(value: String?, color: UIColor?, updateCondition: Condition) {
        self.value = value
        self.color = color
        self.updateCondition = updateCondition
    }
    
    /// Defines wheter the badge should be updated if the tab is the currenlty selected one.
    public enum Condition {
        /// Always update the badge.
        case always
        /// Upadate the badge only then the tab is not selected.
        case onlyWhenUnselected
    }
}
