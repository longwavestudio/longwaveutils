//
//  TabFlowDependency.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 04/05/2020.
//

import UIKit


public protocol TabFlowDependency {
    var flows: [TabFlowEmbeddable] { get }
    var menu: MenuFlow? { get }
}
