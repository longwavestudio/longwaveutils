//
//  TabFlowController.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 04/05/2020.
//

import UIKit

public protocol TabFlowController {
    var tabController: UITabBarController? { get }
    
    func embed(in viewController: UIViewController,
               onCompletion: @escaping (_ info: [String: Any]) -> Void)
    
    func disembed()
}
