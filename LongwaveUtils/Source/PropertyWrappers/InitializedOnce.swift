//
//  InitializedOnce.swift
//  LongwaveUtils
//
//  Created by Massimo Cesaraccio on 26/04/2021.
//

import Foundation

/// Defines  a property that can be initialazed at a stage later than declaration but only once.
/// Accessing the property before initialization or setting it twice will trap.
@propertyWrapper
public struct InitializedOnce<Value> {
    private var storage: Value?
    
    public init() {
        storage = nil
    }
    
    public var wrappedValue: Value {
        get {
            guard storage != nil else { fatalError("accessing uninitialized property") }
            return storage!
        }
        set {
            guard storage == nil else { fatalError("setting an already set once-init property") }
            storage = newValue
        }
    }
}
