//
//  LateInitialized.swift
//  LongwaveUtils
//
//  Created by Massimo Cesaraccio on 26/04/2021.
//

import Foundation

/// Defines  a property that can be initialazed at a stage later than declaration. Accessing the property before initialization will trap.
@propertyWrapper
public struct LateInitialized<Value> {
    private var storage: Value?
    public var projectedValue: LateInitialized<Value> { self }
    
    public init() { storage = nil }
    public init(wrappedValue: Value) { storage = wrappedValue }
    
    public var wrappedValue: Value {
        get {
            guard let value = storage else { fatalError("accessing uninitialized property") }
            return value
        }
        set { storage = newValue }
    }
    
    public var isSet: Bool {
        storage != nil
    }
}
