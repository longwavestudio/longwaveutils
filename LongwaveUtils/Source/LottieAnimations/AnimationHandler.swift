//
//  AnimationHandler.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 07/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public protocol AnimationHandler {
    func play(name: String,
              contentMode: UIView.ContentMode,
              onCompletion completion: ((Bool) -> Void)?)
    func animationView() -> UIView
}
