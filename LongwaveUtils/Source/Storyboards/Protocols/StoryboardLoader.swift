//
//  StoryboardLoader.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

/// Implementation will load viewController from storyboards
public protocol StoryboardLoader {
    /// Retrieve the viewController from the storyboard given the viewController Id
    /// - Parameter id: the viewController Id in the sroryboard
    /// - Parameter finder: The entity that gives id matching among Storyboards and Viewcontrollers
     static func viewController<ViewController>(with id: String, finder: StoryboardFinder) throws -> ViewController
    /// Retrieve the viewController from the storyboard given the viewController Id
    /// - Parameter id: the viewController Id in the sroryboard
    /// - Parameter finder: The entity that gives id matching among Storyboards and Viewcontrollers
    /// - Parameter bundle: The bundle used to retrieve the Storyboard
     static func viewController<ViewController>(with id: String, finder: StoryboardFinder, bundle: Bundle?) throws -> ViewController
}

public extension StoryboardLoader {
    static func viewController<ViewController>(with id: String, finder: StoryboardFinder) throws -> ViewController {
        return try viewController(with: id, finder: finder, bundle: Bundle.main)
    }
}
