//
//  DefaultStoryboardLoader.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

enum StoryboardError: Error {
    case typeNotMatching
    case notFoundInStoryboard
}

/// This class is the default entity capable of retrieving viewControllers from storyboards
public final class DefaultStoryboardLoader {

}

extension DefaultStoryboardLoader: StoryboardLoader {
    /// Retrieve the viewController from the storyboard given the viewController Id
    /// - Parameter id: the viewController Id in the sroryboard
    /// - Parameter finder: The entity that gives id matching among Storyboards and Viewcontrollers
    /// - Parameter bundle: The bundle used to retrieve the Storyboard
    public static func viewController<ViewController>(with id: String,
                                                      finder: StoryboardFinder,
                                                      bundle: Bundle?) throws -> ViewController {
        guard let storyboardId = try? finder.storyboardId(for: id) else {
            throw StoryboardError.notFoundInStoryboard
        }
        let storyBoard = finder.storyboard(for: storyboardId, bundle: bundle)
        guard let viewcontroller = storyBoard.viewController(identifier: id) as? ViewController else {
            throw StoryboardError.typeNotMatching
        }
        return viewcontroller
    }
}


