//
//  UIColort+Colors.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 07/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit


/// RGBA
extension UIColor {
    
    /// Gets RGBA values from a UIColor
    var rgba: ColorComponents {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return ColorComponents(red: red, green: green, blue: blue, alpha: alpha)
    }
}
