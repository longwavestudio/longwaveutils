//
//  UIViewController+Embed.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

enum ViewControllerPresentationError: Error {
    case disembeddingNonEmbeddedController
}

extension UIViewController {
    
    /// Allows viewControllers to embed other viewControllers.
    /// - Note: you should set valid constraint to the viewController view after calling this method.
    ///
    /// - Parameter viewController: the viewController to be embedded
    @discardableResult
    @objc open func embed(viewController: UIViewController, in viewToEmbed: UIView? = nil) -> UIViewController {
        viewController.willMove(toParent: self)
        addChild(viewController)
        (viewToEmbed ?? view).addSubview(viewController.view)
        viewController.didMove(toParent: self)
        return viewController
    }
    
    /// Removes the embeddedViewController that have been previously embed.
    /// - Note: do not call this method if you are not sure you previously embedded the embeddedViewController
    ///
    /// - Parameter embeddedViewController: embeddedViewController description
    @objc open func removeEmbeddedViewController(embeddedViewController: UIViewController) {
        embeddedViewController.willMove(toParent: nil)
        embeddedViewController.view.removeFromSuperview()
        embeddedViewController.removeFromParent()
        embeddedViewController.didMove(toParent: nil)
    }
}
