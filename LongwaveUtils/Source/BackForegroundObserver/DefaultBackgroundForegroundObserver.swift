//
//  DefaultBackgroundForegroundObserver.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public final class DefaultBackgroundForegroundObserver {
    
    private(set) var notificationCenter: NotificationCenter
    private var onBackground: (() -> Void)?
    private var onForeground: (() -> Void)?
    private var onActive: (() -> Void)?
    
    public convenience init() {
        self.init(notificationCenter: NotificationCenter.default)
    }
    
    init(notificationCenter: NotificationCenter) {
        self.notificationCenter = notificationCenter
    }
    
    @objc private func applicationWillEnterForeground() {
        self.onForeground?()
    }
    
    @objc private func applicationDidEnterBackground() {
        self.onBackground?()
    }
    
    @objc private func applicationDidBecomeActiveNotification() {
        self.onActive?()
    }
}

extension DefaultBackgroundForegroundObserver: BackgroundForegroundObserver {
    public func startObserving(onBackground: (() -> Void)?,
                               onForeground: (() -> Void)?,
                               onActive: (() -> Void)?) {
        self.onBackground = onBackground
        self.onForeground = onForeground
        self.onActive = onActive
        
        notificationCenter.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(applicationDidBecomeActiveNotification), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}
