//
//  CustomAlertViewController.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 09/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

final class CustomAlertViewController: UIViewController {
    /// TODO: To be implemented, for the moment we have choose to use the PMAlertController
    /// https://github.com/pmusolino/PMAlertController/blob/master/Library/PMAlertController.swift
    /// Refers to protocol Alertable to make it usable from all the applications that use this functionality
}
