//
//  KeyboardObserver.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

public protocol KeyboardObservable {
    var onDidOpen: ((_ size: CGSize) -> Void)? { get set }
    var onDidClose: ((_ size: CGSize) -> Void)? { get set }
    var onDidChangeFrame: ((_ size: CGSize) -> Void)? { get set }
}

/// Observes the keyboard and notifies when it opens and closes.
/// !!! Warning: should be connected on viewDidLoad or view(Will/Did)Appear. If the keyboard is already opened when loaded the result could be not valid.
public final class KeyboardObserver: KeyboardObservable {
    public var onDidOpen: ((_ size: CGSize) -> Void)?
    public var onDidClose: ((_ size: CGSize) -> Void)?
    public var onDidChangeFrame: ((_ size: CGSize) -> Void)?
    
    private var notificationCenter: NotificationCenter?
    private var keyboardObservers: [NSObjectProtocol] = []
    public var margin: CGFloat = 0

    public init(forNotificationCenter notificationCenter: NotificationCenter = NotificationCenter.default) {
        self.notificationCenter = notificationCenter
        addKeyboardObserver()
    }
    
    deinit {
         removeKeyboardObserver()
    }
    
    /// Adds a keyboard observer  and notify on openBlock closure when the event is triggered
    func addKeyboardObserver() {
        guard let notificationCenter = notificationCenter else { return }
        let o1 = notificationCenter.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                                object: nil,
                                                queue: nil) { [weak self] notification in
            self?.keyBoardDidOpen(with: notification)
        }
        let o2 = notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                                object: nil,
                                                queue: nil) { [weak self] notification in
            self?.keyBoardDidClose(with: notification)
        }
        let o3 = notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                                object: nil,
                                                queue: nil) { [weak self] notification in
            self?.keyBoardDidChangeFrame(with: notification)
        }
        keyboardObservers = [o1, o2, o3]
    }
    
    private func keyBoardDidOpen(with notification: Notification) {
        let keyboardSize = keyboardSizeFromNotification(notification)
        onDidOpen?(keyboardSize)
    }
    
    private func keyBoardDidClose(with notification: Notification) {
        let keyboardSize = self.keyboardSizeFromNotification(notification)
        onDidClose?(keyboardSize)
    }
    
    private func keyBoardDidChangeFrame(with notification: Notification) {
        let keyboardSize = self.keyboardSizeFromNotification(notification)
        onDidChangeFrame?(keyboardSize)
    }
    
    private func keyboardSizeFromNotification(_ notification: Notification) -> CGSize {
        let frameInfo = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]
        guard let keyboardFrame = (frameInfo as? NSValue)?.cgRectValue  else {
            return .zero
        }
        return sizeByAdding(margin: margin, toFrame: keyboardFrame)
    }
    
    private func sizeByAdding(margin: CGFloat, toFrame frame: CGRect) -> CGSize {
        guard margin != 0 else {
            return frame.size
        }
        return CGSize(width: frame.width, height: frame.height + margin)
    }
    
    /// Removes the keyboard observer
    func removeKeyboardObserver() {
        for observer in keyboardObservers {
            notificationCenter?.removeObserver(observer)
        }
        keyboardObservers.removeAll()
    }
}
