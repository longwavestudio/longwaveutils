//
//  KeyboardAdjustable.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

/// Allows the caller to modify the inset according to the keyboard events (open/close)
public protocol KeyboardAdjustable {
    /// To be implemented returning the scrollView that should react to keyboard state changes
    var currentScrollView: UIScrollView? { get }
    /// Moves all the UIComponents according to the open keyboard event
    func reactToKeyboardDidOpen(keyboardSize: CGSize)
    /// Moves all the UIComponents according to the close keyboard event
    func reactToKeyboardDidClose(keyboardSize: CGSize)
}

/// Default implementation that uses the
public extension KeyboardAdjustable {
    func reactToKeyboardDidOpen(keyboardSize: CGSize) {
        guard let currentScrollView = currentScrollView else { return }
        var currenContentInset = currentScrollView.contentInset
        currenContentInset.bottom = keyboardSize.height
        currentScrollView.contentInset = currenContentInset
    }

    func reactToKeyboardDidClose(keyboardSize: CGSize) {
        guard let currentScrollView = currentScrollView else { return }
        var currenContentInset = currentScrollView.contentInset
        currenContentInset.bottom = 0
        currentScrollView.contentInset = currenContentInset
    }
}
