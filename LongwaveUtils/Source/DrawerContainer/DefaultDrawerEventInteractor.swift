//
//  DefaultDrawerEventNotifier.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public final class DefaultDrawerEventInteractor {
    var notifier: Notifier<DrawerEvent> = Notifier()
    var observers: [NotificationObserver] = []
    public init() {}
}

extension DefaultDrawerEventInteractor: DrawerEventSubscribable {
    public func subscribeForDrawerEvent(onEventBlock: @escaping OnDrawerEventBlock) {
        observers.append(notifier.subscribe(onEventBlock))
    }
        
}

extension DefaultDrawerEventInteractor: DrawerEventNotifier {
    public func fireEvent(_ event: DrawerEvent) {
        notifier.notify(event)
    }
}
