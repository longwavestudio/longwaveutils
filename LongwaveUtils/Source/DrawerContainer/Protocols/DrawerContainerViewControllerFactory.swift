//
//  DrawerContainerViewControllerFactory.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public protocol DrawerContainerViewControllerFactory {
    /// Retrieves the container where the menu controller is embedded
    func menuContainerViewController() -> UIViewController & MenuDismissableContainer
    /// Retrieves the menu view controller
    func menuViewController() -> DrawerEventProviderViewController
}
