//
//  DrawerStoryboardFinder.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 10/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

final class DrawerStoryboardFinder: StoryboardFinder {
    let storyboardName = "DrawerMenu"
    
    func storyboardId(for viewControllerId: String) throws -> String {
        switch viewControllerId {
        case "DrawerContainerViewController", "MenuContainerViewController":
            return "Drawer"
        case "DrawerCustomizableMenu":
            return storyboardName
        default:
            throw StoryboardIdError.notFound
        }
        
    }
    
    func viewControllers(inStoryboard id: String) -> [String] {
        switch id {
        case "Drawer":
            return ["DrawerContainerViewController",
                    "MenuContainerViewController"]
        case "DrawerMenu":
            return ["DrawerCustomizableMenu"]
        default:
            return []
        }
    }
}
