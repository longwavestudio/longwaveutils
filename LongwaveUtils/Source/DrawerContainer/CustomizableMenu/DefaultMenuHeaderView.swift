//
//  DefaultMenuHeaderView.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 16/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

final class DefaultMenuHeaderView: UIView {
    var onClose: (() -> Void)?
    @IBOutlet weak var closeButton: UIButton!
    
    func connectButton() {
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
    }
    
    @objc func close() {
        onClose?()
    }
}

extension DefaultMenuHeaderView: CloseProvider {}
