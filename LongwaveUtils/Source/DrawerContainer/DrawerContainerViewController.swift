//
//  DrawerContainerViewController.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 10/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public typealias DrawerEventProviderViewController = UIViewController & DrawerInteractable & CustomizableMenu

public protocol CustomizableMenu {
    func setup(menu: DrawerMenuConfiguration,
               interactable: DrawerEventNotifier)
}

public typealias DrawerEventInteractable = DrawerEventNotifier & DrawerEventSubscribable

public protocol DrawerInteractable {
    var drawerInteractor: DrawerEventNotifier? { get }
}

public enum DrawerContainedType {
    case custom(mainController: DrawerEventProviderViewController)
    case tabbar(controllers: [DrawerEventProviderViewController])
}

let leftMenuScreenCoverage: CGFloat = 0.8

public final class DrawerContainerViewController: UIViewController {

    private(set) var mainController: UIViewController?
    private(set) var menuController: UIViewController?

    private(set) var drawerEventNotifier: DrawerEventInteractable = DefaultDrawerEventInteractor()
    private(set) var viewControllerFactory: DrawerContainerViewControllerFactory = DefaultDrawerContainerViewControllerFactory()
    private(set) var menuConfiguration: DrawerMenuConfiguration?
    
    public func setup(with type: DrawerContainedType,
                      drawerEventNotifier: DrawerEventInteractable = DefaultDrawerEventInteractor(),
                      viewControllerFactory: DrawerContainerViewControllerFactory = DefaultDrawerContainerViewControllerFactory(),
                      menuConfiguration: DrawerMenuConfiguration) {
        self.drawerEventNotifier = drawerEventNotifier
        self.viewControllerFactory = viewControllerFactory
        self.menuConfiguration = menuConfiguration
        let notifier = self.drawerEventNotifier
        notifier.subscribeForDrawerEvent() { [weak self] event in
            self?.handleEvent(event, dismisser: self?.menuController)
        }
        switch type {
        case .custom(let mainController):
            self.mainController = mainController
            embed(viewController: mainController)
        case .tabbar(let controllers):
            let tabbar = UITabBarController()
            self.mainController = tabbar
            tabbar.viewControllers = controllers
            embed(viewController: tabbar)
        }
    }
}

/// Private methods
extension DrawerContainerViewController {
    private func presentMenu() {
        guard let configuration = menuConfiguration else { return }
        let currentMenu = viewControllerFactory.menuViewController()
        currentMenu.setup(menu: configuration,
                          interactable: drawerEventNotifier)
        resizeMenuViewController(menu: currentMenu)
        presentMenu(currentMenu)
    }
    
    private func presentMenu(_ menu: DrawerEventProviderViewController) {
        let menuContainer = viewControllerFactory.menuContainerViewController()
        menuContainer.setup(with: drawerEventNotifier)
        embedMenu(menu, in: menuContainer)
        menuContainer.modalPresentationStyle = .overCurrentContext
        addLateralTransition()
        mainController?.present(menuContainer,
                                animated: true,
                                completion: nil)
    }
    
    private func handleEvent(_ event: DrawerEvent, dismisser: UIViewController?) {
        switch event {
        case .close:
            dismisser?.dismiss(animated: true, completion: nil)
        case .open(_):
            presentMenu()
        }
    }
    
    private func addLateralTransition() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeIn)
        view.window?.layer.add(transition, forKey: kCATransition)
    }
    
    private func resizeMenuViewController(menu: UIViewController) {
        var currentFrame = menu.view.frame.size
        currentFrame.width = currentFrame.width * leftMenuScreenCoverage
        menu.view.frame.size = currentFrame
    }
    
    private func embedMenu(_ menu: UIViewController,
                           in container: UIViewController) {
        container.view.frame = view.frame
        container.embed(viewController: menu)
        container.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.menuController = container
    }
}
