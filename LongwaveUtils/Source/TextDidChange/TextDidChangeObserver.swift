//
//  TextDidChangeObserver.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public protocol TextDidChangeObserver {
    var onTextDidChange: ((_ newTest: String) -> Void)? { get set }
}

extension TextDidChangeObserver {
    public func updatedTextField(_ textField: UITextField,
                                 shouldChangeCharactersIn range: NSRange,
                                 replacementString string: String) -> Bool {
        let updatedText = textField.textFieldUpdatedText(changingCharactersIn: range,
                                                         replacementString: string)
        onTextDidChange?(updatedText)
        return true
    }
}

extension UITextField {
    public func textFieldUpdatedText(changingCharactersIn range: NSRange,
                                     replacementString string: String) -> String {
        let currentText = text ?? ""
        guard let stringRange = Range(range, in: currentText) else {
            return ""
        }
        let updatedText = currentText.replacingCharacters(in: stringRange,
                                                          with: string)
        return updatedText
    }
}
