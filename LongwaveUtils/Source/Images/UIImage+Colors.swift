//
//  UIImage+Colors.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 07/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import CommonCrypto

/// Represents a rect to be painted of  a specific color. Used from UIImage extensioins to draw painted squares on UIImage
struct ColoredSquare {
    var square: CGRect
    var color: UIColor
}

/// Overlaying images
extension UIImage {
    /// Overlay on image on another one
    /// - Parameter otherImage: the image to be overlayed
    /// - Note: If the two images has different sizes the other image will be stretched to match the starting image
    func overlay(otherImage: UIImage) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()!
        context.rotate(by: CGFloat.pi)
        context.translateBy(x: -size.width, y: -size.height)
        context.draw(cgImage!, in: CGRect(origin: CGPoint.zero,
                                          size: size))
        context.draw(otherImage.cgImage!, in: CGRect(origin: CGPoint.zero,
                                                     size: size))
        let myImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return myImage
    }
    
    
}

/// getting colors from UIImage
extension UIImage {
    
    /// Calculates the average color in all the rect sent as parameters
    /// - Parameter rects: the rects we want to calculate average color
    /// - Returns: the list of the ColoredSquare (average color + rect)
    func averageUIColor(in rects: [CGRect]) -> [ColoredSquare] {
        guard let componentsList = averageColor(in: rects) else { return [] }
        var squares: [ColoredSquare] = []
        for colorSquare in componentsList {
            let color = UIColor(red: colorSquare.red / 255, green: colorSquare.green / 255, blue: colorSquare.blue / 255, alpha: colorSquare.alpha / 255)
            squares.append(ColoredSquare(square: colorSquare.rect, color: color))
        }
        return squares
    }
    
    /// Calculates the average color in all the rect sent as parameters
    /// - Parameter rects: the rects we want to calculate average color
    /// - Returns: the list of the ColoredSquare (average color + rect)
    func averageColor(in rects: [CGRect]) -> [(rect: CGRect, red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)]? {
        let context = CIContext(options: [CIContextOption.workingColorSpace : kCFNull!])
        guard let inputImage = self.ciImage ?? CIImage(image: self) else { return nil }
        var colors:[(rect: CGRect, red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)] = []
        for rect in rects {
            guard let filter = CIFilter(name: "CIAreaAverage", parameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: CIVector(cgRect: rect)])
                else { return nil }
            guard let outputImage = filter.outputImage else { return nil }
            var bitmap = [UInt8](repeating: 0, count: 4)
            
            let outputImageRect = CGRect(x: 0, y: 0, width: 1, height: 1)
            context.render(outputImage,
                           toBitmap: &bitmap,
                           rowBytes: 4,
                           bounds: outputImageRect,
                           format: CIFormat.RGBA8,
                           colorSpace: nil)
            colors.append((rect: rect,
                           red: CGFloat(bitmap[0]),
                           green: CGFloat(bitmap[1]),
                           blue: CGFloat(bitmap[2]),
                           alpha: CGFloat(bitmap[3])))
        }
        return colors
    }
    
    /// Calculates the average color of the image
    /// - Returns: the RGBA values of the average color
    func averageColor() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)? {
        guard let inputImage = self.ciImage ?? CIImage(image: self) else { return nil }
        guard let filter = CIFilter(name: "CIAreaAverage",
                                    parameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: CIVector(cgRect: inputImage.extent)])
            else { return nil }
        guard let outputImage = filter.outputImage else { return nil }
        var bitmap = [UInt8](repeating: 0, count: 4)
        let context = CIContext(options: [CIContextOption.workingColorSpace : kCFNull!])
        let outputImageRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        context.render(outputImage,
                       toBitmap: &bitmap,
                       rowBytes: 4,
                       bounds: outputImageRect,
                       format: CIFormat.RGBA8,
                       colorSpace: nil)
        return (red: CGFloat(bitmap[0]),
                green: CGFloat(bitmap[1]),
                blue: CGFloat(bitmap[2]),
                alpha: CGFloat(bitmap[3]))
    }
}

/// drawing colors on UIImage
extension UIImage {
    
    /// Creates an image with a flat background color given the imageCGSize
    /// - Parameters:
    ///   - color: the color to be used as background
    ///   - size: the size of the image (the size of the result is calculated in points so you have to handle the screen scale to get a uiimage of the right size)
    public static func image(with color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return UIImage() }
        return UIImage(cgImage: cgImage)
    }
    
    /// Paints all the sqares contained in squaredColors using the color defined inside the squaredColor itself
    /// - Parameters:
    ///   - squaredColors: the square to be painted and the color to be used
    ///   - stroke: false if the square will be completed colored true if only the perimeter need to be painted
    func drawOnImage(squaredColors: [ColoredSquare], stroke: Bool) -> UIImage? {
        
        UIGraphicsBeginImageContext(size)
        draw(at: CGPoint.zero)
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(5.0)
        context.draw(cgImage!,
                     in: CGRect(origin: CGPoint.zero,
                                size: size))
        for region in squaredColors {
            context.setStrokeColor(region.color.cgColor)
            context.setFillColor(region.color.cgColor)
            let path = UIBezierPath()
            path.stroke(with: .color, alpha: 1)
            var index = 0
            let points = region.square.getBoxPoints()
            for point in points {
                if index == 0 {
                    path.move(to: point)
                    index += 1
                } else {
                    path.addLine(to: point)
                }
            }
            path.lineWidth = 5
            path.close()
            context.addPath(path.cgPath)
            context.drawPath(using: .fill)
        }
        let myImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return myImage
    }
    
}

extension UIImage {
    /// Retrieve pixelbuffer from UIImage
    func toCVPixelBuffer() -> CVPixelBuffer? {
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
                     kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(self.size.width), Int(self.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard status == kCVReturnSuccess else { return nil }
        if let pixelBuffer = pixelBuffer {
            CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
            let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)
            
            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
            let context = CGContext(data: pixelData, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
            
            context?.translateBy(x: 0, y: self.size.height)
            context?.scaleBy(x: 1.0, y: -1.0)
            UIGraphicsPushContext(context!)
            self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
            UIGraphicsPopContext()
            CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
            return pixelBuffer
        }
        return nil
    }
}

extension UIImage{

    public func sha256() -> String{
        if let imageData = cgImage?.dataProvider?.data as Data? {
            return hexStringFromData(input: digest(input: imageData as NSData))
        }
        return ""
    }

    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }

    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }

        return hexString
    }
}
