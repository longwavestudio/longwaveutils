//
//  CGRectTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 07/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class CGRectTests: XCTestCase {
    
    func testRectZeroHasFourPoint() {
        let rect = CGRect.zero
        let points = rect.getBoxPoints()
        XCTAssertEqual(points.count, 4)
    }
    
    func testRectZeroHasFourPointInZeroZero() {
        let rect = CGRect.zero
        let points = rect.getBoxPoints()
        XCTAssertEqual(points[0], CGPoint.zero)
        XCTAssertEqual(points[1], CGPoint.zero)
        XCTAssertEqual(points[2], CGPoint.zero)
        XCTAssertEqual(points[3], CGPoint.zero)
        
    }
    
    func testRectInZeroZeroAsOriginHasFourPoint() {
        let width: CGFloat = 30
        let heigth: CGFloat = 30
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: heigth))
        let points = rect.getBoxPoints()
        XCTAssertEqual(points[0], CGPoint.zero)
        XCTAssertEqual(points[1], CGPoint(x: width, y: 0))
        XCTAssertEqual(points[2], CGPoint(x: width, y: heigth))
        XCTAssertEqual(points[3], CGPoint(x: 0, y: heigth))
        
    }
    
    func testRectOriginHasFourPoint() {
        let x: CGFloat = 10
        let y: CGFloat = 15
        let width: CGFloat = 30
        let heigth: CGFloat = 30
        let rect = CGRect(origin: CGPoint(x: x, y: y), size: CGSize(width: width, height: heigth))
        let points = rect.getBoxPoints()
        XCTAssertEqual(points[0], CGPoint(x: x, y: y))
        XCTAssertEqual(points[1], CGPoint(x: x + width, y: y))
        XCTAssertEqual(points[2], CGPoint(x: x + width, y: y + heigth))
        XCTAssertEqual(points[3], CGPoint(x: x, y: y + heigth))
        
    }
    
}
