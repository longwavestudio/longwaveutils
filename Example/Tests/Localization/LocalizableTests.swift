//
//  LocalizableTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 17/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class LocalizableTests: XCTestCase {
    
    func testCiaoItalian() {
        XCTAssertEqual("ciao".localizedString(in: Bundle(for: LocalizableTests.self)), "ciao")
    }
    func testCiaoItalianNoBunble() {
        XCTAssertEqual("ciao".localizedString(), "ciao")
    }
    
    /// check why it is not working
//    func testCiaoEnglish() {
//        XCTAssertEqual("ciao".localizedString(in: Bundle(for: LocalizableTests.self)), "hello")
//    }

}
