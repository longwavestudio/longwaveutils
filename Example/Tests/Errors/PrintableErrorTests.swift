//
//  PrintableErrorTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest

@testable import LongwaveUtils
private enum TestPrintableError {
    case testError
}

extension TestPrintableError: PrintableError {
    func getErrorMessage() -> String {
        return "hey"
    }
}

class PrintableErrorTests: XCTestCase {

     func testDefaultTitleIsNil() {
        XCTAssertNil(TestPrintableError.testError.getErrorTitle())
    }
    
    func testPresentationShouldNotPresentIfPresenterIsNil() {
        let sut = TestPrintableError.testError
        XCTAssertNil(sut.showError(on: nil))
    }
    
    func testPresentationShouldReturnTheAlertControllerIfPresenterIsNotNil() {
        let sut = TestPrintableError.testError
        let presenter = UIViewController()
        XCTAssertNotNil(sut.showError(on: presenter))
    }
    
    func testPresentationShouldPresentIfPresenterIsNotNil() {
        let sut = TestPrintableError.testError
        let presenter = ViewControllerMock()
        var presentationDidCall = false
        presenter.onPresentDidCall = { viewController in
            presentationDidCall = true
        }
        sut.showError(on: presenter)
        XCTAssertTrue(presentationDidCall)
    }
    
    func testAlertShouldContainDefaultOkActionIfNoActionIsSet() {
        let sut = TestPrintableError.testError
        let presenter = UIViewController()
        let errorAlert = sut.showError(on: presenter)
        XCTAssertEqual(errorAlert?.actions.count, 1)
    }
    
    func testAlertShouldContainSetActionsActionIfActionsAreSet() {
        let sut = TestPrintableError.testError
        let presenter = UIViewController()
        let action1 = UIAlertAction(title: "1", style: .default, handler: nil)
        let action2 = UIAlertAction(title: "2", style: .default, handler: nil)
        let errorAlert = sut.showError(on: presenter, actions: [action1, action2])
        XCTAssertEqual(errorAlert?.actions.count, 2)
        XCTAssertEqual(errorAlert?.actions[0].title, "1")
        XCTAssertEqual(errorAlert?.actions[1].title, "2")
    }
}
