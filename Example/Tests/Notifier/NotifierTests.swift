//
//  NotifierTests.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class NotifierTests: XCTestCase {

    private var observer: NotificationObserver?
    
    override func tearDown() {
        observer = nil
    }
    
    func testNotifierSubscriptionShouldSucceed() {
        let notifier: Notifier<String> = Notifier()
        let testString = "testString"
        let exp = expectation(description: "testNotifierSubscriptionShouldSucceed")
        observer = notifier.subscribe { key in
            XCTAssertEqual(key, testString)
            exp.fulfill()
        }
        DispatchQueue.main.async {
            notifier.notify(testString)
        }
        waitForExpectations(timeout: 1)
    }

    func testNotifierSubscriptionBeDeallocatedImmediately() {
        let notifier: Notifier<String> = Notifier()
        let testString = "testString"
        let exp = expectation(description: "testNotifierSubscriptionBeDeallocatedImmediately")
        _ = notifier.subscribe { key in
            XCTFail("should not be called")
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            exp.fulfill()
        }
        DispatchQueue.main.async {
            notifier.notify(testString)
        }
        waitForExpectations(timeout: 1)
    }
    
    func testNotifierSubscriptionAndUnsubscriptionShouldSucceed() {
        let notifier: Notifier<String> = Notifier()
        let testString = "testString"
        let exp = expectation(description: "testNotifierSubscriptionAndUnsubscriptionShouldSucceed")
        let currentObserver = notifier.subscribe { key in
            exp.fulfill()
        }
        observer = currentObserver
        notifier.unsubscribe(observer: currentObserver)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            exp.fulfill()
        }
        DispatchQueue.main.async {
            notifier.notify(testString)
        }
        waitForExpectations(timeout: 1)
    }
    
}
