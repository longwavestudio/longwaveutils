//
//  ViewControllerMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

class ViewControllerMock: UIViewController {
    var onViewDidLoad: (() -> Void)?
    var onWillMove: ((UIViewController?) -> Void)?
    var onPresentDidCall: ((UIViewController?) -> Void)?
    
    override func viewDidLoad() {
        onViewDidLoad?()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        onWillMove?(parent)
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        onPresentDidCall?(viewControllerToPresent)
    }
}
