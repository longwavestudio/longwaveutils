//
//  DrawerCustomizableMenuTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 16/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

typealias Sut = DrawerCustomizableMenu
private typealias Loader = DefaultStoryboardLoader

class DrawerCustomizableMenuTests: XCTestCase {

    private func getSut(shouldLoadView: Bool = true) -> Sut {
        let finder = DrawerStoryboardFinder()
        let currentbundle = Bundle(for: DrawerCustomizableMenu.self)
        let vcId = "DrawerCustomizableMenu"
        let sut: Sut = try! Loader.viewController(with: vcId,
                                                  finder: finder,
                                                  bundle: currentbundle)
        if shouldLoadView {
            _ = sut.view
        }
        return sut
    }
    
    func testSetupShouldLoadHeaderIfPresent() {
        let sut = getSut()
        final class Header: UIView & CloseProvider {
            var onClose: (() -> Void)?
        }
        let headerExpected = Header()
        let interactor = DrawerEventNotifierMock()
        let dependency = DrawerMenuConfiguration(header: headerExpected,
                                                 sections: [])
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertNotNil(sut.menuTableView.tableHeaderView)
    }
    
    func testSetupShouldSetDefaultHeaderIfNotPresent() {
        let sut = getSut()
        let interactor = DrawerEventNotifierMock()
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: [])
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertNotNil(sut.menuTableView.tableHeaderView)
        XCTAssertTrue(sut.menuTableView.tableHeaderView is DefaultMenuHeaderView)

    }
    
    func testSetupWithNoSectionShouldReturnZeroSections() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: [])
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertEqual(sut.numberOfSections(in: sut.menuTableView), 0)
    }

    func testSetupWithSectionsShouldReturnNumberOfSectionsAccordingly() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let sections = [DrawerMenuSection(title: "a", elements: []),
                        DrawerMenuSection(title: "b", elements: [])]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertEqual(sut.numberOfSections(in: sut.menuTableView), 2)
    }
    
    func testNumberOfRowsInSectionIsZeroIfNoItemIsPresent() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let sections = [DrawerMenuSection(title: "a", elements: []),
                        DrawerMenuSection(title: "b", elements: [])]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertEqual(sut.tableView(sut.menuTableView,
                                     numberOfRowsInSection: 0), 0)
    }
    
    func testNumberOfRowsInSectionIsSetAccordingToElementsInSection() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: "saaa",
                                          onItemDidTap: nil),
                           DrawerMenuItem(image: nil,
                                          title: "sbbb",
                                          onItemDidTap: nil),
                           DrawerMenuItem(image: nil,
                                          title: "sccc",
                                          onItemDidTap: nil)]
        let sections = [DrawerMenuSection(title: "a", elements: drawerItems)]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertEqual(sut.tableView(sut.menuTableView,
                                     numberOfRowsInSection: 0), 3)
    }
    
    func testNumberOfRowsInSectionOutOfBoundsIsZero() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: "saaa",
                                          onItemDidTap: nil),
                           DrawerMenuItem(image: nil,
                                          title: "sbbb",
                                          onItemDidTap: nil),
                           DrawerMenuItem(image: nil,
                                          title: "sccc",
                                          onItemDidTap: nil)]
        let sections = [DrawerMenuSection(title: "a", elements: drawerItems)]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        XCTAssertEqual(sut.tableView(sut.menuTableView,
                                     numberOfRowsInSection: 1), 0)
    }
    
    func testCellForRowShouldSucceedReturningUITableViewCell() {
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: "saaa",
                                          onItemDidTap: nil)]
        let cell = getMenuButtonItemCell(for: drawerItems)
        XCTAssertNotNil(cell)
    }
    
    func testCellForRowShouldReturnMenuItemTableViewCell() {
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: "saaa",
                                          onItemDidTap: nil)]
        let cell = getMenuButtonItemCell(for: drawerItems)
        XCTAssertTrue(cell is MenuButtonItemTableViewCell)
    }
    
    func testClosingFromHeaderShoukdNotifyClose() {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let sections = [DrawerMenuSection(title: "a", elements: []),
                        DrawerMenuSection(title: "b", elements: [])]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        var closeDidCall = false
        interactor.subscribeForDrawerEvent() { _ in
            closeDidCall = true
        }
        (sut.menuTableView.tableHeaderView as? DefaultMenuHeaderView)?.close()
        XCTAssertTrue(closeDidCall)
    }
}

/// MenuButtonItemTableViewCell
extension DrawerCustomizableMenuTests {
    private func getMenuButtonItemCell(for drawerItems: [DrawerMenuItem]) -> UITableViewCell {
        let sut = getSut()
        _ = sut.view
        let interactor = DrawerEventNotifierMock()
        let sections = [DrawerMenuSection(title: "a", elements: drawerItems)]
        let dependency = DrawerMenuConfiguration(header: nil,
                                                 sections: sections)
        sut.setup(menu: dependency, interactable: interactor)
        let cell = sut.tableView(sut.menuTableView,
                                 cellForRowAt: IndexPath(row: 0,
                                                         section: 0))
        return cell
    }
    
    func testLeadingImageIsSetInCellIfPresent() {
        let image = UIImage.image(with: .red,
                                  size: CGSize(width: 20,
                                               height: 20))
        let drawerItems = [DrawerMenuItem(image: image,
                                          title: "saaa",
                                          onItemDidTap: nil)]
        let cell = getMenuButtonItemCell(for: drawerItems)
        XCTAssertEqual((cell as? MenuButtonItemTableViewCell)?.leadingImage.image, image)
    }
    
    func testLeadingImageIsNilSetInCellIfNotPresent() {
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: "saaa",
                                          onItemDidTap: nil)]
        let cell = getMenuButtonItemCell(for: drawerItems) as? MenuButtonItemTableViewCell
        XCTAssertNil(cell?.leadingImage.image)
    }
    
    func testCellSetupShouldSetTheTitle() {
        let expectedText = "saaa"
        let drawerItems = [DrawerMenuItem(image: nil,
                                          title: expectedText,
                                          onItemDidTap: nil)]
        let cell = getMenuButtonItemCell(for: drawerItems) as? MenuButtonItemTableViewCell
        XCTAssertEqual(cell?.buttonTitleLabel.text, expectedText)
    }
}
