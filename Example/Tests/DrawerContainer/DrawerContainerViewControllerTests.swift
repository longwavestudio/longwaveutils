//
//  DrawerContainerViewControllerTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 10/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class DrawerContainerViewControllerTests: XCTestCase {

    func getSut(finder: StoryboardFinder = DrawerStoryboardFinder(),
                currentBundle: Bundle = Bundle(for: DrawerStoryboardFinder.self)) -> DrawerContainerViewController? {
        let finder = finder
        let sut: DrawerContainerViewController?
        let viewControllerId = "DrawerContainerViewController"
        sut = try? DefaultStoryboardLoader.viewController(with: viewControllerId,
                                                          finder: finder,
                                                          bundle: currentBundle)
        return sut
    }
    
    func testContainerShouldEmbedMainController() {
        let sut = getSut()
        let mainController = DrawerMainControllerMock()
        let configuration = DrawerMenuConfiguration(header: nil,
                                                    sections: [])
        sut?.setup(with: .custom(mainController: mainController),
                   menuConfiguration: configuration)
        XCTAssertNotNil(sut?.children.first(where: { $0 is DrawerMainControllerMock }))
    }
    
    private func setupSut(type: DrawerContainedType,
                          drawerEventNotifier: DrawerEventInteractable = DrawerEventNotifierMock()) -> (sut: DrawerContainerViewController?, menuController: MenuControllerMock, menuContainer: UIViewController, eventNotifier: DrawerEventNotifierMock) {
        let sut = getSut()
        let factory = DrawerViewControllerFactoryMock()
        let menuControllerMock = MenuControllerMock()
        factory.onMenuRequested = {
            return menuControllerMock
        }
        let menuContainer = MenuContainerMock()
        factory.onMenuContainerRequested = {
            return menuContainer
        }
        let configuration = DrawerMenuConfiguration(header: nil,
                                                    sections: [])
        sut?.setup(with: type,
                   drawerEventNotifier: drawerEventNotifier,
                   viewControllerFactory: factory,
                   menuConfiguration: configuration)
        return (sut: sut,
                menuController: menuControllerMock,
                menuContainer: menuContainer,
                eventNotifier: drawerEventNotifier as! DrawerEventNotifierMock)
    }
    
    func testMenuShouldBePresentedOnMainController() {
        let mainController = DrawerMainControllerMock()
        let sutComponents = setupSut(type: .custom(mainController: mainController))
        var presentationDidCall = false
        mainController.onPresent = { vc in
            presentationDidCall = true
        }
        sutComponents.eventNotifier.fireEvent(.open(.left))
        XCTAssertTrue(presentationDidCall)
    }
    
    func testMenuIsEmbeddedInsideAContainer() {
        let mainController = DrawerMainControllerMock()
        let sutComponents = setupSut(type: .custom(mainController: mainController))
        var presentationDidCall = false
        mainController.onPresent = { vc in
            presentationDidCall = true
            XCTAssertNotNil(vc.children.first(where: { $0 is MenuControllerMock }))
        }
        sutComponents.eventNotifier.fireEvent(.open(.left))
        XCTAssertTrue(presentationDidCall)
    }
    
    func testEnbeddedMenuVCIsResizedToCover80PerCentOfTheScreen() {
        let mainController = DrawerMainControllerMock()
        let sutComponents = setupSut(type: .custom(mainController: mainController))
        sutComponents.eventNotifier.fireEvent(.open(.left))
        let expextedScreen = UIScreen.main.bounds.width * 0.8
        XCTAssertEqual(sutComponents.menuController.view.frame.width, expextedScreen)
    }
    
    func testMenuContainerBackgroundIsBlackWithAlpha() {
        let mainController = DrawerMainControllerMock()
        let sutComponents = setupSut(type: .custom(mainController: mainController))
        var presentationDidCall = false
        mainController.onPresent = { vc in
            presentationDidCall = true
            XCTAssertEqual(vc.view.backgroundColor, UIColor.black.withAlphaComponent(0.3))
        }
        sutComponents.eventNotifier.fireEvent(.open(.left))
        XCTAssertTrue(presentationDidCall)
    }
    
    func testMainControllerIsSetToATabbarIfTypeIsTabbar() {
        let sut = getSut()
        let mainController = DrawerMainControllerMock()
        let configuration = DrawerMenuConfiguration(header: nil,
                                                    sections: [])
        sut?.setup(with: .tabbar(controllers: [mainController]), menuConfiguration: configuration)
        XCTAssertNotNil(sut?.children.first(where: { $0 is UITabBarController }))
    }
    
    func testViewControllersAreSetIntoTheTabbar() {
        let sut = getSut()
        let mainController = DrawerMainControllerMock()
        let configuration = DrawerMenuConfiguration(header: nil,
                                                    sections: [])
        sut?.setup(with: .tabbar(controllers: [mainController]), menuConfiguration: configuration)
        let tabbar = sut?.children.first(where: { $0 is UITabBarController }) as? UITabBarController
        XCTAssertEqual(tabbar?.viewControllers, [mainController])
    }
    
    func testContainerShouldSubscribeForDrawerEvents() {
        let mainController = DrawerMainControllerMock()
        let notifierMock = DrawerEventNotifierMock()
        var subscribeDidCall = false
        notifierMock.onSubscribeForDrawerEvent = { _ in
            subscribeDidCall = true
        }
        let _ = setupSut(type: .custom(mainController: mainController), drawerEventNotifier: notifierMock)
        XCTAssertTrue(subscribeDidCall)
    }
    
    func testContainerDetectCloseEventsAndCloseTheMenu() {
        let mainController = DrawerMainControllerMock()
        let sutComponents = setupSut(type: .custom(mainController: mainController))
        sutComponents.eventNotifier.fireEvent(.open(.left))
        var dismissDidCall = false
        (sutComponents.menuContainer as? MenuContainerMock)?.onDismiss = {
            dismissDidCall = true
        }
        sutComponents.eventNotifier.fireEvent(.close)
        XCTAssertTrue(dismissDidCall)
    }
    
   
}

/// DrawerStoryboardFinder
extension DrawerContainerViewControllerTests {
    func testFinderShouldReturnTwoViewControllerForDrawer() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "Drawer").count, 2)
    }
    
    func testStoryboardIdForDrawerContainerViewControllerIsDrawer() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(try? sut.storyboardId(for: "DrawerContainerViewController"), "Drawer")
    }
    
    func testStoryboardIdForMenuContainerViewControllerIsDrawer() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(try? sut.storyboardId(for: "MenuContainerViewController"), "Drawer")
    }
    
    func testStoryboardIdForDrawerCustomizableMenuIsDrawerMenu() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(try? sut.storyboardId(for: "DrawerCustomizableMenu"), "DrawerMenu")
    }
    
    func testWrongIdShouldThrowAnStoryboardIdError() {
        let sut = DrawerStoryboardFinder()
        XCTAssertThrowsError(try sut.storyboardId(for: "aaa"))
    }
    
    func testFinderShouldReturnOneViewControllerForDrawerMenu() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "DrawerMenu").count, 1)
    }
    
    func testFinderShouldReturnZeroViewControllerForUnknown() {
        let sut = DrawerStoryboardFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "sddd").count, 0)
    }
}

extension DrawerContainerViewControllerTests {
    func testDefaultDrawerContainerViewControllerFactoryMenuContainerIsCreated() {
        let sut = DefaultDrawerContainerViewControllerFactory()
        XCTAssertNotNil(sut.menuContainerViewController())
    }
    
    func testDefaultDrawerContainerViewControllerFactoryMenuIsCreated() {
        let sut = DefaultDrawerContainerViewControllerFactory()
        XCTAssertNotNil(sut.menuViewController())
    }
}

/// MenuContainer
extension DrawerContainerViewControllerTests {
    
    func testGestureRecognizerHAsBeenAddedToOnTap() {
        let factory = DefaultDrawerContainerViewControllerFactory()
        let sut = factory.menuContainerViewController()
        let notifier = DrawerEventNotifierMock()
        (sut as? MenuContainerViewController)?.setup(with: notifier)
        let gestureView = sut.view.subviews.first
        XCTAssertNotNil(gestureView?.gestureRecognizers?.first)
    }
    
    func testMenuContainerShouldNotifyDismissIfTappedOnTheGrayArea() {
        let factory = DefaultDrawerContainerViewControllerFactory()
        let sut = factory.menuContainerViewController()
        let notifier = DrawerEventNotifierMock()
        (sut as? MenuContainerViewController)?.setup(with: notifier)
        var didReceiveEvent = false
        notifier.subscribeForDrawerEvent() { _ in
            didReceiveEvent = true
        }
        (sut as? MenuContainerViewController)?.onTap()
        XCTAssertTrue(didReceiveEvent)
    }
}
/// DrawerEvent
extension DrawerContainerViewControllerTests {
    
    func testOpenLeftEventIsEqualToOpenLeftEvent() {
        let openLeft = DrawerEvent.open(.left)
        let openLeft2 = DrawerEvent.open(.left)
        XCTAssertEqual(openLeft, openLeft2)
    }
    
    func testOpenLeftEventIsEqualToOpenRightEvent() {
        let openLeft = DrawerEvent.open(.left)
        let openLeft2 = DrawerEvent.open(.right)
        XCTAssertEqual(openLeft, openLeft2)
    }
    
    func testOpenEventIsNotEqualToCloseEvent() {
        let openLeft = DrawerEvent.open(.left)
        let close = DrawerEvent.close
        XCTAssertNotEqual(openLeft, close)
    }
    
    func testCloseEventIsEqualToCloseEvent() {
        let close = DrawerEvent.close
        let close2 = DrawerEvent.close
        XCTAssertEqual(close2, close)
    }
}
