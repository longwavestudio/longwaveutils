//
//  DefaultDrawerEventInteractorTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 16/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class DefaultDrawerEventInteractorTests: XCTestCase {
    
    func testWhenEventIsFiredSubscribedCallbackShouldBeTriggered() {
        let sut = DefaultDrawerEventInteractor()
        let exp = expectation(description: "oneEventOneSubscription")
        sut.subscribeForDrawerEvent() { event in
            switch event {
            case .open(let position):
                XCTAssertEqual(position, .left)
                break
            case .close:
                XCTFail("wrong event")
            }
            exp.fulfill()
        }
        sut.fireEvent(.open(.left))
        waitForExpectations(timeout: 0.1)
    }

    func testWhenEventIsFiredMultipleSubscribedCallbacksShouldBeTriggered() {
        let sut = DefaultDrawerEventInteractor()
        let exp = expectation(description: "oneEventMultipleSubscription")
        let group = DispatchGroup()
        sut.subscribeForDrawerEvent() { event in
            switch event {
            case .open(let position):
                XCTAssertEqual(position, .left)
                break
            case .close:
                XCTFail("wrong event")
            }
            group.leave()
        }
        group.enter()
        sut.subscribeForDrawerEvent() { event in
            switch event {
            case .open(let position):
                XCTAssertEqual(position, .left)
                break
            case .close:
                XCTFail("wrong event")
            }
            group.leave()
        }
        group.enter()
        group.notify(queue: DispatchQueue.main) {
            exp.fulfill()
        }
        sut.fireEvent(.open(.left))
        waitForExpectations(timeout: 0.1)
    }
}
