//
//  MenuContainerMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 13/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils

final class MenuContainerMock: UIViewController {
    var onDismiss: (() -> Void)?
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        onDismiss?()
    }
    
    func triggerDismiss() {
        onDismiss?()
    }
}
extension MenuContainerMock: MenuDismissableContainer {
    func setup(with notifier: DrawerEventNotifier) {
        
    }
}
