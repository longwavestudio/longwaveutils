//
//  DrawerEventNotifierMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
@testable import LongwaveUtils

final class DrawerEventNotifierMock: DrawerEventSubscribable & DrawerEventNotifier {
    var onSubscribeForDrawerEvent: ((_ onEventBlock: OnDrawerEventBlock) -> Void)?
    
    var callBack: OnDrawerEventBlock?
    func subscribeForDrawerEvent(onEventBlock: @escaping OnDrawerEventBlock) {
        onSubscribeForDrawerEvent?(onEventBlock)
        callBack = onEventBlock
    }
    
    func fireEvent(_ event: DrawerEvent) {
        callBack?(event)
    }
}
