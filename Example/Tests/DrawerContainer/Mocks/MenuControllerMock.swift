//
//  MenuControllerMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 13/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils

final class MenuControllerMock: UIViewController {
    var drawerInteractor: DrawerEventNotifier?
    func setup(menu: DrawerMenuConfiguration, interactable: DrawerEventNotifier) {
        
    }
}

extension MenuControllerMock: DrawerInteractable {}
extension MenuControllerMock: CustomizableMenu {}
