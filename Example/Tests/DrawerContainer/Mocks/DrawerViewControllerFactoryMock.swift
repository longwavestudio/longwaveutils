//
//  DrawerViewControllerFactoryMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils

final class DrawerViewControllerFactoryMock: DrawerContainerViewControllerFactory {
    var onMenuRequested: (() -> DrawerEventProviderViewController)!
    var onMenuContainerRequested: (() -> UIViewController & MenuDismissableContainer)!

    func menuContainerViewController() -> UIViewController & MenuDismissableContainer {
        return onMenuContainerRequested()
    }
    
    func menuViewController() -> DrawerEventProviderViewController {
        return onMenuRequested()
    }
}

