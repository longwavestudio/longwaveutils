//
//  DrawerMainControllerMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 13/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils

final class DrawerMainControllerMock: UIViewController {
    var onPresent: ((UIViewController) -> Void)?
    var notifier: DrawerEventSubscribable?
    var drawerInteractor: DrawerEventNotifier?
    
    func setup(menu: DrawerMenuConfiguration, interactable: DrawerEventNotifier) {
        
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        onPresent?(viewControllerToPresent)
    }
}

extension DrawerMainControllerMock: DrawerInteractable {}
extension DrawerMainControllerMock: CustomizableMenu {}
