//
//  TextDidChageDefaultBehaviorTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class TextDidChageDefaultBehaviorTests: XCTestCase {

    func testOnTextDidChangeCallbackIsCalledWithVoidStringOnCellIfRangeNotValid() {
        let sut = TestTextField(frame: CGRect.zero)
        sut.tf.text = "aaa"
        var callbackDidCall = false
        sut.onTextDidChange = { newText in
            XCTAssertEqual(newText, "")
            callbackDidCall = true
        }
        _ = sut.textField(sut.tf,
                      shouldChangeCharactersIn: NSRange(location: 10, length: 1), replacementString: "a")
        XCTAssertTrue(callbackDidCall)
    }
    
    func testOnTextDidChangeCallbackIsCalledOnCell() {
        let sut = TestTextField(frame: CGRect.zero)
        sut.tf.text = "aaa"
        var callbackDidCall = false
        sut.onTextDidChange = { newText in
            XCTAssertEqual(newText, "baaa")
            callbackDidCall = true
        }
        _ = sut.textField(sut.tf,
                      shouldChangeCharactersIn: NSRangeFromString("b"), replacementString: "b")
        XCTAssertTrue(callbackDidCall)
    }
}

final class TestTextField: UIView {
    var onTextDidChange: ((String) -> Void)?
    let tf = UITextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tf)
        tf.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension TestTextField: TextDidChangeObserver, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        updatedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
}

