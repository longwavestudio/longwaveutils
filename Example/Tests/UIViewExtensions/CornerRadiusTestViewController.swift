//
//  CornerRadiusTestViewController.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

final class CornerRadiusTestViewController: UIViewController {
    static var identifier = "CornerRadiusTestViewController"
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var cornerLabel: UILabel!
    @IBOutlet weak var cornerButton: UIButton!
}
