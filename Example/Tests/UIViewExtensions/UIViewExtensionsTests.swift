//
//  UIViewExtensionsTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class UIViewExtensionsTests: XCTestCase {
    
    func testElevateShouldSetLayerMaskToBounds() {
        let view = UIView()
        view.elevate(elevation: 1, opacity: 0.5)
        XCTAssertFalse(view.layer.masksToBounds)
    }
    
    func testElevateShouldSetLayerShadowOpacity() {
        let view = UIView()
        view.elevate(elevation: 1, opacity: 0.5)
        XCTAssertEqual(view.layer.shadowOpacity, 0.5)
    }
    
    func testElevateShouldSetLayerShadowColor() {
        let view = UIView()
        view.elevate(elevation: 1, opacity: 0.5)
        XCTAssertEqual(view.layer.shadowColor, UIColor.black.cgColor)
    }
    
    func testElevateShouldSetLayerShadowOffset() {
        let view = UIView()
        view.elevate(elevation: 1, opacity: 0.5)
        XCTAssertEqual(view.layer.shadowOffset, CGSize(width: 0, height: 1))
    }
    
    func testElevateShouldSetLayerShadowRadius() {
        let view = UIView()
        view.elevate(elevation: 1, opacity: 0.5)
        XCTAssertEqual(view.layer.shadowRadius, 1)
    }
    
    func testNotificationObserverEqualityWithSameUUIDShouldBeTrue() {
        let notifier: Notifier<String> =  Notifier()
        let observer = NotificationObserver(notifier: notifier)
        XCTAssertEqual(observer, observer)
    }
    
    func testNotificationObserverEqualityWithDifferentUUIDShouldBeFalse() {
        let notifier: Notifier<String> =  Notifier()
        let observer1 = NotificationObserver(notifier: notifier)
        let observer2 = NotificationObserver(notifier: notifier)
        XCTAssertNotEqual(observer1, observer2)
    }
}
