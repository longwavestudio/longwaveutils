//
//  CornerRadiusExtensionsTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

private typealias Controller = CornerRadiusTestViewController
private typealias Loader = DefaultStoryboardLoader
private typealias Finder = CornerRadiusTestsStoryboardFinder

class CornerRadiusExtensionsTests: XCTestCase {

    private func getSut() -> Controller {
        let finder = Finder()
        let sut: Controller = try! Loader.viewController(with: Controller.identifier, finder: finder, bundle: Bundle(for: Controller.self))
        _ = sut.view
        return sut
    }
    
    func testViewCornerRadiusIsSetTo10InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerView.cornerRadius, 10)
    }
    
    func testViewBorderWidthIsSetTo1InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerView.borderWidth, 1)
    }
    
    func testViewBorderColorIsSetToBlackInStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerView.borderColor, UIColor.black)
    }
    
    func testLabelCornerRadiusIsSetTo10InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerLabel.cornerRadius, 10)
    }
    
    func testLabelBorderWidthIsSetTo1InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerLabel.borderWidth, 1)
    }
    
    func testLabelBorderColorIsSetToBlackInStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerLabel.borderColor, UIColor.black)
    }
    
    func testButtonCornerRadiusIsSetTo10InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerButton.cornerRadius, 10)
    }
    
    func testButtonBorderWidthIsSetTo1InStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerButton.borderWidth, 1)
    }
    
    func testButtonBorderColorIsSetToBlackInStoryboard() throws {
        let sut = getSut()
        XCTAssertEqual(sut.cornerButton.borderColor, UIColor.black)
    }
}
