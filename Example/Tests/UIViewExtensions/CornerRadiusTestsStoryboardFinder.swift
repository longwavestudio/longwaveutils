//
//  CornerRadiusTestsStoryboardFinder.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
@testable import LongwaveUtils

final class CornerRadiusTestsStoryboardFinder {
    var storyboardName: String = "CornerRadiusTests"
}

extension CornerRadiusTestsStoryboardFinder: StoryboardFinder {
    func storyboardId(for viewControllerId: String) throws -> String {
        return storyboardName
    }
    
    func viewControllers(inStoryboard id: String) -> [String] {
        return []
    }
    
    
}
