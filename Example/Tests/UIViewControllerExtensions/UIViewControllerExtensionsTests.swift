//
//  UIViewControllerExtensionsTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class UIViewControllerExtensionsTests: XCTestCase {
    
    func testEmbeddingAViewControllerShouldCallViewDidLoad() {
        let presenter = ViewControllerMock()
        let sut = ViewControllerMock()
        var viewDidLoad = false
        sut.onViewDidLoad = {
            viewDidLoad = true
        }
        presenter.embed(viewController: sut)
        XCTAssertTrue(viewDidLoad)
    }
    
    func testDisembeddingAViewControllerShouldReleaseViewController() {
        let presenter = ViewControllerMock()
        let sut = ViewControllerMock()
        sut.onViewDidLoad = {
            presenter.removeEmbeddedViewController(embeddedViewController: sut)
        }
        var viewDidDisembed = false
        sut.onWillMove = { vc in
            viewDidDisembed = true
        }
        presenter.embed(viewController: sut)
        XCTAssertTrue(viewDidDisembed)
    }

}
