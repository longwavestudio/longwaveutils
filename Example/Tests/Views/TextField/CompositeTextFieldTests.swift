//
//  CompositeTextFieldTests.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 28/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

private typealias Test = CompositeTextFieldTests

enum PrintableTestError: PrintableError {
    
    case generic
    
    func getErrorMessage() -> String {
        return "generic"
    }
}

class CompositeTextFieldTests: XCTestCase {
    
    static let deleteImage = UIImage.image(with: .blue)
    static let checkOkImage = UIImage.image(with: .green)
    static let checkKoImage = UIImage.image(with: .brown)
    static let secureInvisibleImage = UIImage.image(with: .cyan)
    static let secureVisibleImage = UIImage.image(with: .purple)

    private static func getSutTheme() -> CompositeTextFieldTheme {
        let titleLabelTheme = LabelTheme(labelFont: UIFont.boldSystemFont(ofSize: 15),
                                         labelColor: UIColor.darkGray)
        let textFieldTheme = TextFieldTheme(textFont: UIFont.boldSystemFont(ofSize: 15),
                                            textColor: UIColor.darkGray,
                                            textFieldBorderColor: UIColor.green,
                                            textFieldErrorBorderColor: UIColor.red,
                                            placeholderFont: UIFont.boldSystemFont(ofSize: 15),
                                            placeholderColor: UIColor.gray,
                                            deleteImage: deleteImage,
                                            checkOkImage: checkOkImage,
                                            checkKoImage: checkKoImage,
                                            secureInvisibleImage: secureInvisibleImage,
                                            secureVisibleImage: secureVisibleImage)
        let errorLabelTheme = LabelTheme(labelFont: UIFont.systemFont(ofSize: 15),
                                         labelColor: UIColor.red)
        let theme = CompositeTextFieldTheme(titleLabelTheme: titleLabelTheme,
                                            textField: textFieldTheme,
                                            errorLabelTheme: errorLabelTheme)
        return theme
    }
    
    private static func getSut(theme: CompositeTextFieldTheme?) -> CompositeTextField {
        guard let currentTheme = theme  else {
            let titleLabelTheme = LabelTheme(labelFont: UIFont.boldSystemFont(ofSize: 15),
                                             labelColor: UIColor.darkGray)
            let textFieldTheme = TextFieldTheme(textFont: UIFont.systemFont(ofSize: 15),
                                                textColor: UIColor.darkGray,
                                                textFieldBorderColor: UIColor.green,
                                                textFieldErrorBorderColor: UIColor.red,
                                                placeholderFont: UIFont.systemFont(ofSize: 15),
                                                placeholderColor: UIColor.gray,
                                                deleteImage: UIImage.image(with: .blue),
                                                checkOkImage: UIImage.image(with: .green),
                                                checkKoImage: UIImage.image(with: .brown),
                                                secureInvisibleImage: UIImage.image(with: .cyan),
                                                secureVisibleImage: UIImage.image(with: .purple))
            let errorLabelTheme = LabelTheme(labelFont: UIFont.systemFont(ofSize: 15),
                                             labelColor: UIColor.red)
            let theme = CompositeTextFieldTheme(titleLabelTheme: titleLabelTheme,
                                                textField: textFieldTheme,
                                                errorLabelTheme: errorLabelTheme)
            return CompositeTextField.create(theme: theme)
        }
        return CompositeTextField.create(theme: currentTheme)
    }
    
    func test_compositeTextField_shouldBeLoadedFromXib() throws {
        let sut = Test.getSut(theme: nil)
        XCTAssertNotNil(sut)
    }
    
    func test_stackView_afterLoad_isNotNil() {
        let sut = Test.getSut(theme: nil)
        XCTAssertNotNil(sut.stackView)
    }

    func test_titleLabel_afterLoad_shouldBeContainedInsideStackView() {
        let sut = Test.getSut(theme: nil)
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.titleLabel))
    }

    func test_textField_afterLoad_shouldBeContainedInsideStackView() {
        let sut = Test.getSut(theme: nil)
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.textFieldContainer))
        XCTAssertTrue(sut.textFieldContainer.contains(sut.currentTextField))
    }
    
    func test_errorLabel_afterLoad_shouldNotBeContainedInsideStackView() {
        let sut = Test.getSut(theme: nil)
        XCTAssertFalse(sut.stackView.arrangedSubviews.contains(sut.errorLabel))
    }
    
    func test_errorLabel_onSetupWithError_shouldNotContainedInsideStackView() {
        let sut = Test.getSut(theme: nil)
        sut.updateState(.contentNotOk(error: PrintableTestError.generic))
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.errorLabel))
    }
    
    func test_errorLabel_onErrorSet_ShouldBeSetWithThePrintableErrorTitleString() {
        let sut = Test.getSut(theme: nil)
        sut.updateState(.contentNotOk(error: PrintableTestError.generic))
        XCTAssertEqual(sut.errorLabel.text, "generic")
    }
    
    func test_titleLabelFont_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.titleLabel.font, theme.titleLabelTheme.labelFont)
    }
    
    func test_textFieldTextFont_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.currentTextField.font, theme.textField.textFont)
    }
    
    func test_textFieldPlaceholderTextFont_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        let attributes = sut.currentTextField.attributedPlaceholder?.attributes(at: 0,
                                                                         effectiveRange: nil)
        XCTAssertNotNil(attributes?[NSAttributedString.Key.font])
    }
    
    func test_errorLabelFont_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.errorLabel.font, theme.errorLabelTheme.labelFont)
    }
    
    
    func test_titleLabelColor_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.titleLabel.textColor, theme.titleLabelTheme.labelColor)
    }
    
    func test_textFieldTextColor_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.currentTextField.textColor, theme.textField.textColor)
    }
    
    func test_textFieldPlaceholderTextColor_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        let attributes = sut.currentTextField.attributedPlaceholder?.attributes(at: 0,
                                                                         effectiveRange: nil)
        XCTAssertNotNil(attributes?[NSAttributedString.Key.foregroundColor])
    }
    
    func test_errorLabelColor_afterLoad_isSetAccordingToTheTheme() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.errorLabel.textColor, theme.errorLabelTheme.labelColor)
    }
    
    func test_errorLabel_onStatusOk_isRemoved() {
        let sut = Test.getSut(theme: nil)
        sut.updateState(.contentNotOk(error: PrintableTestError.generic))
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.errorLabel))
        sut.updateState(.contentOk)
        XCTAssertFalse(sut.stackView.arrangedSubviews.contains(sut.errorLabel))
    }
    
    func test_TFRightButton_withNoText_isNotVisible() {
        let sut = Test.getSut(theme: nil)
        XCTAssertNil(sut.currentTextField.rightView)
    }
    
    func test_TFRightButton_withText_isVisible() {
        let sut = Test.getSut(theme: nil)
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 0),
                          replacementString: "a")
        XCTAssertNotNil(sut.currentTextField.rightView)
    }
    
    func test_TFRightButton_withText_containsCancelImage() {
        let sut = Test.getSut(theme: nil)
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 0),
                          replacementString: "a")
        XCTAssertEqual((sut.currentTextField.rightView as? UIButton)?.image(for: .normal)?.sha256(), Test.deleteImage.sha256())
    }
    
    func test_TFRightButton_withTextDeleted_isNotVisible() {
        let sut = Test.getSut(theme: nil)
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 0),
                          replacementString: "a")
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 1),
                          replacementString: "")
        XCTAssertNil(sut.currentTextField.rightView)
    }
    
    
    func test_TFRightButtonWithCancel_onTap_shouldDeleteTextInTextField() {
        let sut = Test.getSut(theme: nil)
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 0),
                          replacementString: "a")
        (sut.currentTextField.rightView as? UIButton)?.sendAction(#selector(CompositeTextField.deleteText),
                                                                  to: sut,
                                                                  for: UIEvent())
        XCTAssertEqual(sut.currentTextField.text, "")
    }
    
    func test_TFRightButton_ifTypeIsSecureText_shouldBeAlwaisVisible() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        XCTAssertNotNil(sut.currentTextField.rightView)
    }
    
    func test_textField_ifTypeIsNotSecureText_shouldNotBeSetToTextSecured() {
        let sut = Test.getSut(theme: nil)
        XCTAssertFalse(sut.currentTextField.isSecureTextEntry)
    }
    
    func test_textField_ifTypeIsSecureText_shouldBeSetToTextSecured() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        XCTAssertTrue(sut.currentTextField.isSecureTextEntry)
    }
    
    func test_TFRightButton_ifTypeIsSecureText_shouldBeFilledWithEyeIcon() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        XCTAssertEqual((sut.currentTextField.rightView as? UIButton)?.image(for: .normal)?.sha256(), Test.secureVisibleImage.sha256())
    }
    
    func test_TFRightButton_ifTypeIsSecureTextAndWriteAChar_shouldStillBeFilledWithEyeIcon() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        _ = sut.textField(sut.currentTextField,
                          shouldChangeCharactersIn: NSRange(location: 0,
                                                            length: 0),
                          replacementString: "a")
        XCTAssertEqual((sut.currentTextField.rightView as? UIButton)?.image(for: .normal)?.sha256(), Test.secureVisibleImage.sha256())
    }
    
    func test_TFRightButtonWithEye_onTap_shouldMakeTextVisible() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        (sut.currentTextField.rightView as? UIButton)?.sendAction(#selector(CompositeTextField.makeTextSecured),
                                                                  to: sut,
                                                                  for: UIEvent())
        XCTAssertFalse(sut.currentTextField.isSecureTextEntry)
    }
    
    func test_TFRightButtonWithEye_onTap_shouldChangeButtonIcon() {
        let sut = Test.getSut(theme: nil)
        sut.type = .secureText
        (sut.currentTextField.rightView as? UIButton)?.sendAction(#selector(CompositeTextField.makeTextSecured),
                                                                  to: sut,
                                                                  for: UIEvent())
        XCTAssertEqual((sut.currentTextField.rightView as? UIButton)?.image(for: .normal)?.sha256(), Test.secureInvisibleImage.sha256())
    }
    
    func test_TFRightView_onContentOk_shouldBeSetToContentOkImage() {
        let sut = Test.getSut(theme: nil)
        sut.updateState(.contentOk)
        XCTAssertEqual((sut.currentTextField.rightView as? UIImageView)?.image?.sha256(), Test.checkOkImage.sha256())
    }
    
    func test_TFRightView_onContentKO_shouldBeSetToContentOkImage() {
        let sut = Test.getSut(theme: nil)
        sut.updateState(.contentNotOk(error: PrintableTestError.generic))
        XCTAssertEqual((sut.currentTextField.rightView as? UIImageView)?.image?.sha256(), Test.checkKoImage.sha256())
    }
    
    func test_viewWithTypeReadOnlyTextWithInput_onLoad_shouldContainAButton() {
        let sut = Test.getSut(theme: nil)
        sut.type = .readOnlyTextWithInput
        XCTAssertTrue(sut.textFieldContainer.subviews.contains(where: { $0 is UIButton }))
    }
    
    func test_viewWithTypeReadOnlyTextWithInput_onTap_shouldCallOnInputRequired() {
        let sut = Test.getSut(theme: nil)
        sut.type = .readOnlyTextWithInput
        var didCallInputRequired = false
        sut.onInputRequired = { block in
            didCallInputRequired = true
        }
        let button = sut.textFieldContainer.subviews.first(where: { $0 is UIButton })
        (button as? UIButton)?.sendAction(#selector(CompositeTextField.inputRequired),
                                          to: sut,
                                          for: UIEvent())
        XCTAssertTrue(didCallInputRequired)
    }
    
    func test_viewWithTypeReadOnlyTextWithInput_onInputcompleted_ShouldSetTextInTextField() {
        let sut = Test.getSut(theme: nil)
        sut.type = .readOnlyTextWithInput
        let expectedText = "qwertyuio"
        sut.onInputRequired = { block in
            block(expectedText)
        }
        let button = sut.textFieldContainer.subviews.first(where: { $0 is UIButton })
        (button as? UIButton)?.sendAction(#selector(CompositeTextField.inputRequired),
                                          to: sut,
                                          for: UIEvent())
        XCTAssertEqual(sut.currentTextField.text, expectedText)
    }
    
    func test_TextFieldBorder_afterLoad_shouldBeSetToTextFieldBorderColor() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        XCTAssertEqual(sut.textFieldContainer.layer.borderColor, theme.textField.textFieldBorderColor.cgColor)
    }
    
    func test_TextFieldBorder_onError_shouldBeSetToTextFieldErrorBorderColor() {
        let theme = Test.getSutTheme()
        let sut = Test.getSut(theme: theme)
        sut.updateState(.contentNotOk(error: PrintableTestError.generic))
        XCTAssertEqual(sut.textFieldContainer.layer.borderColor, theme.textField.textFieldErrorBorderColor.cgColor)
    }
}
