//
//  ColorsTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 07/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class ColorsTests: XCTestCase {

    func testImageWithRedColorCreationShouldCreateAOnePixelImage() {
        let colorImage = UIImage.image(with: .red)
        XCTAssertNotNil(colorImage)
        let scale = UIScreen.main.scale
        XCTAssertEqual(colorImage.size, CGSize(width: 1 * scale, height: 1 * scale)) //this is 2 because of the scale of the image
    }

    func testImageWithRedColorCreationShouldSucceed() {
        let colorImage = UIImage.image(with: .red)
        XCTAssertNotNil(colorImage)
    }

    func testImageWithRedColorCreationShouldSucceedCreatingRedImage() {
        let colorImage = UIImage.image(with: .red)
        XCTAssertEqual(colorImage.averageColor()?.red, 255)
        XCTAssertEqual(colorImage.averageColor()?.green, 0)
        XCTAssertEqual(colorImage.averageColor()?.blue, 0)
        XCTAssertEqual(colorImage.averageColor()?.alpha, 255)
    }
    
    func testAverageColorInRectShouldSucceed() {
        let colorImage = UIImage.image(with: .red, size: CGSize(width: 50, height: 50))
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: 25, height: 25))
        let color = colorImage.averageColor(in: [rect])?.first
        XCTAssertEqual(color?.red, 255)
        XCTAssertEqual(color?.green, 0)
        XCTAssertEqual(color?.blue, 0)
        XCTAssertEqual(color?.alpha, 255)
    }
    
    func testTransformUIImageToPixelBufferShouldSucceed() {
        let colorImage = UIImage.image(with: .red, size: CGSize(width: 50, height: 50))
        XCTAssertNotNil(colorImage.toCVPixelBuffer())
    }
    
    func testColorComponentsUIColorShouldReturnRightColor() {
        let colorComponents1 = ColorComponents(red: 255,
                                               green: 0,
                                               blue: 0,
                                               alpha: 255)
        let color = colorComponents1.uiColor()
        let colorComponents = color.rgba
        XCTAssertEqual(colorComponents.red, 1)
        XCTAssertEqual(colorComponents.green, 0)
        XCTAssertEqual(colorComponents.blue, 0)
        XCTAssertEqual(colorComponents.alpha, 1)
    }
    
    func testAverageUIColorShouldSucceed() {
        let size = CGSize(width: 50, height: 50)
        let colorImage = UIImage.image(with: ColorComponents(red: 255, green: 0, blue: 0, alpha: 255).uiColor(), size: size)
        let rect = CGRect(origin: CGPoint.zero, size: size)
        let color = colorImage.averageUIColor(in: [rect]).first?.color.rgba
        XCTAssertEqual(color?.red, 1)
        XCTAssertEqual(color?.green, 0)
        XCTAssertEqual(color?.blue, 0)
        XCTAssertEqual(color?.alpha, 1)
    }
    
    func testDrawingInsideImageShouldSucceed() {
        let size = CGSize(width: 200, height: 200)
        let colorImage = UIImage.image(with: .red,
                                       size: size)
        let rect = CGRect(origin: CGPoint(x: 30,
                                          y: 30),
                          size: CGSize(width: 30,
                                       height: 30))
        let internalRect = CGRect(origin: CGPoint(x: 31,
                                                  y: 31),
                                  size: CGSize(width: 28,
                                               height: 28))
        let newImage = colorImage.drawOnImage(squaredColors: [ColoredSquare(square: rect, color: .green)], stroke: false)
        let testCroppedImage = UIImage(cgImage: (newImage?.cgImage?.cropping(to: internalRect))!)
        let color = testCroppedImage.averageColor()
        XCTAssertEqual(color?.red, 0)
        XCTAssertEqual(color?.green, 255)
        XCTAssertEqual(color?.blue, 0)
        XCTAssertEqual(color?.alpha, 255)
    }
    
    func testOverlayingAGreenImageOnRedImageShouldSucceed() {
        let image = UIImage.image(with: .red, size: CGSize(width: 100, height: 100))
        let overlayImage = UIImage.image(with: .green, size: CGSize(width: 30, height: 30))
        let sut = image.overlay(otherImage: overlayImage)
        let internalRect = CGRect(origin: CGPoint.zero,
                                  size: CGSize(width: 30,
                                               height: 30))
        let testCroppedImage = UIImage(cgImage: (sut?.cgImage?.cropping(to: internalRect))!)
        let color = testCroppedImage.averageColor()
        XCTAssertEqual(color?.red, 0)
        XCTAssertEqual(color?.green, 255)
        XCTAssertEqual(color?.blue, 0)
        XCTAssertEqual(color?.alpha, 255)
    }
    
    func testImagesShouldBeEqual() {
        let size = CGSize(width: 50, height: 50)
        let imageOn1 = UIImage.image(with: ColorComponents(red: 255, green: 0, blue: 0, alpha: 255).uiColor(), size: size)
        let imageOn2 = UIImage.image(with: ColorComponents(red: 255, green: 0, blue: 0, alpha: 255).uiColor(), size: size)
        let sha1 = imageOn1.sha256()
        let sha2 = imageOn2.sha256()
        XCTAssertEqual(sha1, sha2)
    }
    
    func testImagesShouldNotBeEqual() {
        let size = CGSize(width: 50, height: 50)
        let imageOn1 = UIImage.image(with: ColorComponents(red: 255, green: 200, blue: 0, alpha: 255).uiColor(), size: size)
        let imageOn2 = UIImage.image(with: ColorComponents(red: 255, green: 0, blue: 0, alpha: 255).uiColor(), size: size)
        let sha1 = imageOn1.sha256()
        let sha2 = imageOn2.sha256()
        XCTAssertNotEqual(sha1, sha2)
    }
    
    func testShaIsNotRetrievedOnImage() {
        let imageOn1 = UIImage()
        let sha1 = imageOn1.sha256()
        XCTAssertEqual(sha1, "")
    }

}

