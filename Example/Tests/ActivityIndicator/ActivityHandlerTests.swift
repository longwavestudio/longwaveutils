//
//  ActivityHandlerTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class ActivityHandlerTests: XCTestCase {

    func testActivityShouldStart() {
       let activity = TestActivityHandler()
        activity.startActivity()
        XCTAssertTrue(activity.activityIndicator?.isAnimating ?? false)
    }

    func testActivityShouldStop() {
       let activity = TestActivityHandler()
        activity.startActivity()
        XCTAssertTrue(activity.activityIndicator?.isAnimating ?? false)
        activity.stopActivity()
        XCTAssertFalse(activity.activityIndicator?.isAnimating ?? false)
    }
}

final class TestActivityHandler: ActivityHandler {
    
    var activityIndicator: UIActivityIndicatorView? = UIActivityIndicatorView()
    
}
