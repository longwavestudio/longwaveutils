//
//  KeyboardObserverMock.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//


import UIKit
@testable import LongwaveUtils
final class KeyboardObserverMock {
    var onDidOpen: ((CGSize) -> Void)?
    var onDidClose: ((CGSize) -> Void)?
    var onDidChangeFrame: ((_ size: CGSize) -> Void)?
    
    func open(withSize size: CGSize) {
        onDidOpen?(size)
    }
    
    func close() {
        onDidClose?(.zero)
    }
}

extension KeyboardObserverMock: KeyboardObservable {}
