//
//  KeyboardAdjustableTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class KeyboardAdjustableTests: XCTestCase {

    func testIfKeyboardHeightIsMoreThanZeroBottomInsetIsModifiedAddingTheHeightOnOpen() {
        class TestKeyboardAdjuster: KeyboardAdjustable {
            
            var currentScrollView: UIScrollView? { scrollView }
            let scrollView = UIScrollView(frame: CGRect(origin: CGPoint.zero,
            size: CGSize(width: 100,
                         height: 100)))
        }
        
        let expectedHeight: CGFloat = 30
        let keyboardSize = CGSize(width: 30, height: expectedHeight)
        let sut = TestKeyboardAdjuster()
        let scrollView = sut.scrollView
        XCTAssertEqual(scrollView.contentInset.bottom, 0)
        sut.reactToKeyboardDidOpen(keyboardSize: keyboardSize)
        XCTAssertEqual(scrollView.contentInset.bottom, expectedHeight)
    }

    func testIfKeyboardHeightIsMoreThanZeroBottomInsetIsModifiedRemovingTheHeightOnClose() {
        class TestKeyboardAdjuster: KeyboardAdjustable {
            var currentScrollView: UIScrollView? { scrollView }
            let scrollView = UIScrollView(frame: CGRect(origin: CGPoint.zero,
                                                        size: CGSize(width: 100,
                                                                     height: 100)))
        }       
        let expectedHeight: CGFloat = 0
        let currentBottomInset: CGFloat = 30
        let keyboardSize = CGSize(width: 30, height: currentBottomInset)
        let sut = TestKeyboardAdjuster()
        let scrollView = sut.scrollView
        var inset = scrollView.contentInset
        inset.bottom = currentBottomInset
        scrollView.contentInset = inset
        XCTAssertEqual(scrollView.contentInset.bottom, currentBottomInset)
        sut.reactToKeyboardDidClose(keyboardSize: keyboardSize)
        XCTAssertEqual(scrollView.contentInset.bottom, expectedHeight)
    }
    
}
