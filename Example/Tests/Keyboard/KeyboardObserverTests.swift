//
//  KeyboardObserverTests.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//


import XCTest
@testable import LongwaveUtils

class KeyboardObserverTests: XCTestCase {
    
    var observer: KeyboardObserver?
    
    override func tearDown() {
        observer = nil
        super.tearDown()
    }
    
    func testKeyboardObserverShouldCallOpenBlockWhenNotificationIsReceived() {
        let exp = expectation(description: "testKeyboardObserverShouldCallOpenBlockWhenNotificationIsReceived")
        let notificationCenter = NotificationCenter()
        observer = KeyboardObserver(forNotificationCenter: notificationCenter)
        observer?.onDidOpen = { size in
            XCTAssertNotNil(notificationCenter)
            exp.fulfill()
        }
        notificationCenter.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: [UIResponder.keyboardFrameEndUserInfoKey: CGRect(x: 0, y: 0, width: 320, height: 200)])
        waitForExpectations(timeout: 1)
    }
    
    func testKeyboardObserverShouldCallCloseBlockWhenNotificationIsReceived() {
        let exp = expectation(description: "testKeyboardObserverShouldCallCloseBlockWhenNotificationIsReceived")
        let notificationCenter = NotificationCenter()
        observer = KeyboardObserver(forNotificationCenter: notificationCenter)
        observer?.onDidClose = { _ in 
            XCTAssertNotNil(notificationCenter)
            exp.fulfill()
        }
        notificationCenter.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: [UIResponder.keyboardFrameEndUserInfoKey: CGRect(x: 0, y: 0, width: 320, height: 200)])
        notificationCenter.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: nil)
        waitForExpectations(timeout: 1)
    }
    
    func testKeyboardObserverShouldCallFrameChangedWhenNotificationIsReceived() {
        let exp = expectation(description: "testKeyboardObserverShouldCallFrameChangedWhenNotificationIsReceived")
        let notificationCenter = NotificationCenter()
        observer = KeyboardObserver(forNotificationCenter: notificationCenter)
        observer?.onDidChangeFrame = { _ in
            XCTAssertNotNil(notificationCenter)
            exp.fulfill()
        }
        notificationCenter.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: [UIResponder.keyboardFrameEndUserInfoKey: CGRect(x: 0, y: 0, width: 320, height: 200)])
        notificationCenter.post(name: UIResponder.keyboardDidChangeFrameNotification, object: nil, userInfo: [UIResponder.keyboardFrameEndUserInfoKey: CGRect(x: 0, y: 0, width: 320, height: 400)])
        waitForExpectations(timeout: 1)
    }
    
    func testOpenNotCalledWithNoFrameAssociatedToKeyboard() {
        let exp = expectation(description: "testOpenNotCalledWithNoFrameAssociatedToKeyboard")
        let notificationCenter = NotificationCenter()
        observer = KeyboardObserver(forNotificationCenter: notificationCenter)
        observer?.onDidOpen = { size in
            XCTAssertEqual(size, CGSize.zero)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            exp.fulfill()
        }
        notificationCenter.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: [:])
        waitForExpectations(timeout: 1)
    }
}
