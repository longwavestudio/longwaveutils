//
//  DefaultStoryboardLoaderTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class DefaultStoryboardLoaderTests: XCTestCase {

    func testStoryboardRetrievingSucceed() {
        let storyBoardFinder = StoryboardFinderMock()
        let storyboardId = "testStoryboard"
        var storyboardFinderStoryboardIdCalled = false
        storyBoardFinder.onStoryboardIdRequested = { viewControllerId in
            storyboardFinderStoryboardIdCalled = true
            return storyboardId
        }
        let vcToRet = UIViewController()
        storyBoardFinder.onStoryboardRetrieverRequested = {
            return StoryboardRetrieverMock { _ in
                return vcToRet
            }
        }
        XCTAssertEqual(try DefaultStoryboardLoader.viewController(with: "a", finder: storyBoardFinder), vcToRet)
        XCTAssertTrue(storyboardFinderStoryboardIdCalled)
    }
    
    func testStoryboardRetrievingFailsOnMismatchingTypes() {
        
        final class TestVC: UIViewController {}
        final class TestVC2: UIViewController { }
        let storyBoardFinder = StoryboardFinderMock()
        let storyboardId = "testStoryboard"
        var storyboardFinderStoryboardIdCalled = false
        storyBoardFinder.onStoryboardIdRequested = { viewControllerId in
            storyboardFinderStoryboardIdCalled = true
            return storyboardId
        }
        storyBoardFinder.onStoryboardRetrieverRequested = {
            return StoryboardRetrieverMock { _ in
                return TestVC()
            }
        }
        var sut: TestVC2?
        do {
            sut  = try? DefaultStoryboardLoader.viewController(with: "a", finder: storyBoardFinder)
        }
        XCTAssertNil(sut)
        XCTAssertTrue(storyboardFinderStoryboardIdCalled)
    }
    
    func testDefaultStoryboardRetrieverIsUIStoryboard() {
        class StoryboardFinderTest: StoryboardFinder {
            var storyboardName: String = ""
            
            func storyboardId(for viewControllerId: String) -> String {
                return ""
            }
            
            func viewControllers(inStoryboard id: String) -> [String] {
                return []
            }
        }
        let storyboard = StoryboardFinderTest().storyboard(for: "Test",
                                              bundle: Bundle(for: DefaultStoryboardLoaderTests.self))
        XCTAssertTrue(storyboard is UIStoryboard)
    }
    
    func testStoryboardRetrievingIscoonnectedToUIStoryboardByDefault() {
        let storyboard = UIStoryboard(name: "Test",
                                      bundle: Bundle(for: DefaultStoryboardLoaderTests.self))
        let vc = storyboard.viewController(identifier: "Test")
        XCTAssertNotNil(vc)
    }
    
    func testLoaderThrowsErrorIfViewcontrollerIsNotInTheStoryboard() {
        let finder = StoryboardFinderMock()
        finder.onStoryboardIdRequested = { _ in
            throw StoryboardError.notFoundInStoryboard
        }
        XCTAssertThrowsError(try DefaultStoryboardLoader.viewController(with: "aaa",
        finder: finder) as UIViewController)
    }
}
