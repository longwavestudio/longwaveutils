//
//  StoryboardFinderAndRetrieverMock.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils

final class StoryboardFinderMock {
    var storyboardName: String = ""
    var onStoryboardIdRequested: ((_ viewController: String) throws -> String)?
    var onViewControllersRequeted: ((_ viewController: String) -> [String])?
    var onStoryboardRetrieverRequested: (() -> StoryboardRetriever)!
    
}
extension StoryboardFinderMock: StoryboardFinder {
    func storyboardId(for viewControllerId: String) throws -> String {
        return try onStoryboardIdRequested?(viewControllerId) ?? ""
    }
    
    func viewControllers(inStoryboard id: String) -> [String] {
        return onViewControllersRequeted?(id) ?? []
    }
    
    func storyboard(for id: String, bundle: Bundle?) -> StoryboardRetriever {
        return onStoryboardRetrieverRequested()
    }
}


final class StoryboardRetrieverMock<T> where T : UIViewController {
    
    var onViewcontrollerRequested: (_ identifier: String) -> T
    init(onViewcontrollerRequested: @escaping (_ identifier: String) -> T) {
        self.onViewcontrollerRequested = onViewcontrollerRequested
    }
}

extension StoryboardRetrieverMock: StoryboardRetriever {
    func viewController<T>(identifier: String) -> T where T : UIViewController {
        return onViewcontrollerRequested(identifier) as! T
    }
}
