//
//  StringCommonExtensionTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class StringCommonExtensionTests: XCTestCase {
    
    private func time(fromHours hours: Int64, minutes: Int64, seconds: Int64) -> TimeInterval {
        return TimeInterval(hours * 3600 + minutes * 60 + seconds)
    }
    
    func testZero() {
        let t = time(fromHours: 0, minutes: 0, seconds: 0)
        let s = String.stringFromPlaybackTime(t)
        let expected = "00:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlySeconds() {
        let t = time(fromHours: 0, minutes: 0, seconds: 24)
        let s = String.stringFromPlaybackTime(t)
        let expected = "00:24"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlySecondsSingleDigit() {
        let t = time(fromHours: 0, minutes: 0, seconds: 1)
        let s = String.stringFromPlaybackTime(t)
        let expected = "00:01"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyMinutes() {
        let t = time(fromHours: 0, minutes: 12, seconds: 0)
        let s = String.stringFromPlaybackTime(t)
        let expected = "12:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyMinutesSingleDigit() {
        let t = time(fromHours: 0, minutes: 1, seconds: 0)
        let s = String.stringFromPlaybackTime(t)
        let expected = "01:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyHoursSingleDigit() {
        let t = time(fromHours: 2, minutes: 0, seconds: 0)
        let s = String.stringFromPlaybackTime(t)
        let expected = "2:00:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyHoursMultiDigit() {
        let t = time(fromHours: 17, minutes: 0, seconds: 0)
        let s = String.stringFromPlaybackTime(t)
        let expected = "17:00:00"
        XCTAssertEqual(expected, s)
    }
    
    func testAll() {
        let t = time(fromHours: 17, minutes: 23, seconds: 44)
        let s = String.stringFromPlaybackTime(t)
        let expected = "17:23:44"
        XCTAssertEqual(expected, s)
    }
    
    func testAllSingleDigit() {
        let t = time(fromHours: 4, minutes: 23, seconds: 44)
        let s = String.stringFromPlaybackTime(t)
        let expected = "4:23:44"
        XCTAssertEqual(expected, s)
    }
    
    func testMinutesSeconds() {
        let t = time(fromHours: 0, minutes: 23, seconds: 44)
        let s = String.stringFromPlaybackTime(t)
        let expected = "23:44"
        XCTAssertEqual(expected, s)
    }
    
    func testLessThanHour() {
        let t = time(fromHours: 0, minutes: 59, seconds: 59)
        let s = String.stringFromPlaybackTime(t)
        let expected = "59:59"
        XCTAssertEqual(expected, s)
    }
    
    func testExactlyOneHour() {
        let t = time(fromHours: 0, minutes: 59, seconds: 59) + 1
        let s = String.stringFromPlaybackTime(t)
        let expected = "1:00:00"
        XCTAssertEqual(expected, s)
    }
    
    func testMoreThanOneHour() {
        let t = time(fromHours: 1, minutes: 32, seconds: 30)
        let s = String.stringFromPlaybackTime(t)
        let expected = "1:32:30"
        XCTAssertEqual(expected, s)
    }
    
    func testZeroFullString() {
        let t = time(fromHours: 0, minutes: 0, seconds: 0)
        let s = String.stringFromPlaybackTime(t, alwaysFull: true)
        let expected = "00:00:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyHoursSingleDigitFullString() {
        let t = time(fromHours: 2, minutes: 15, seconds: 0)
        let s = String.stringFromPlaybackTime(t, alwaysFull: true)
        let expected = "02:15:00"
        XCTAssertEqual(expected, s)
    }
    
    func testOnlyHoursSingleDigitShortExplicitParam() {
        let t = time(fromHours: 2, minutes: 15, seconds: 0)
        let s = String.stringFromPlaybackTime(t, alwaysFull: false)
        let expected = "2:15:00"
        XCTAssertEqual(expected, s)
    }
    
    func testIsValidEmailShouldReturnTrueOnValidEmail() {
        let email = "ciccio@ciccio.com"
        XCTAssertTrue(email.isValidEmail())
    }
    
    func testIsValidEmailShouldReturnFalseOnInvalidEmailMissingDomain() {
        let emailMissingDomain = "ciccio@"
        XCTAssertFalse(emailMissingDomain.isValidEmail())
    }
    
    func testIsValidEmailShouldReturnFalseOnInvalidEmailMissingFirstPart() {
        let emailMissingFirstPart = "@ciccio.com"
        XCTAssertFalse(emailMissingFirstPart.isValidEmail())
    }
    
    func testIsValidEmailShouldReturnFalseOnInvalidEmailMissingSeparator() {
        let emailMissingSeparator = "ciccio.com"
        XCTAssertFalse(emailMissingSeparator.isValidEmail())
    }
    
    func testIsValidEmailShouldReturnFalseOnInvalidEmailMissingExtension() {
        let emailMissingExtension = "ciccio@ciccio"
        XCTAssertFalse(emailMissingExtension.isValidEmail())
    }
    
    func testIsValidEmailShouldReturnFalseOnInvalidEmailDomainOneLetter() {
        let emailDomainOneLetter = "ciccio@ciccio.l"
        XCTAssertFalse(emailDomainOneLetter.isValidEmail())
    }
    
    // less than 9, only numbers -> false
    func test_phoneValidation_forShortNumbers_shouldFail() {
        XCTAssertFalse("123456789".isValidPhoneNumber())
    }
    
    // more than 9, numbers and chars -> false
    func test_phoneValidation_forNumbersAndChars_shouldFail() {
        XCTAssertFalse("1234ab567890".isValidPhoneNumber())
    }
    
    // more than 9, numbers and special -> false
    func test_phoneValidation_forNumbersAndSpecialChars_shouldFail() {
        XCTAssertFalse("1234567!@89".isValidPhoneNumber())
    }
    
    // more than 9, only numbers -> true
    func test_phoneValidation_longNumbers_shouldPass() {
        XCTAssertTrue("1234567890".isValidPhoneNumber())
    }
    
    // more than 9, only numbers and plus -> true
    func test_phoneValidation_longNumbers_andPlus_shouldPass() {
        XCTAssertTrue("+123456789".isValidPhoneNumber())
    }
    
    // more than 9, only numbers and too many pluses -> false
    func test_phoneValidation_longNumbers_andPluses_shouldFail() {
        XCTAssertFalse("++123456789".isValidPhoneNumber())
    }
    
    // more than 9, only numbers with trailing plus -> false
    func test_phoneValidation_longNumbers_andTrailingPlus_shouldFail() {
        XCTAssertFalse("++123456789".isValidPhoneNumber())
    }
}
