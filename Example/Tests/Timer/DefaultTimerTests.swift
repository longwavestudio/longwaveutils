//
//  DefaultTimerTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 06/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class DefaultTimerTests: XCTestCase {

    func testTimerShouldFireIn02Seconds() throws {
        let timer = DefaultTimer()
        var didFireEvent = false
        let exp = expectation(description: "testTimerShouldFireIn02Seconds")
        timer.eventHandler = {
            didFireEvent = true
        }
        DispatchQueue.main.async {
            XCTAssertFalse(didFireEvent)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            XCTAssertTrue(didFireEvent)
            exp.fulfill()
        }
        timer.resume(timeInterval: 0.1)
        waitForExpectations(timeout: 0.3)
    }

    func testTimerSuspendedShouldNotFireIn02Seconds() throws {
        let timer = DefaultTimer()
        var didFireEvent = false
        let exp = expectation(description: "testTimerSuspendedShouldNotFireIn02Seconds")
        timer.eventHandler = {
            didFireEvent = true
        }
        DispatchQueue.main.async {
            timer.suspend()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            XCTAssertFalse(didFireEvent)
            exp.fulfill()
        }
        timer.resume(timeInterval: 0.1)
        waitForExpectations(timeout: 0.3)
    }
    
    func testCallingSuspendedTwiceShouldNotHaveEffect() throws {
        let timer = DefaultTimer()
        var didFireEvent = false
        let exp = expectation(description: "testCallingSuspendedTwiceShouldNotHaveEffect")
        timer.eventHandler = {
            didFireEvent = true
        }
        DispatchQueue.main.async {
            timer.suspend()
            timer.suspend()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            XCTAssertFalse(didFireEvent)
            exp.fulfill()
        }
        timer.resume(timeInterval: 0.1)
        waitForExpectations(timeout: 0.3)
    }
}
