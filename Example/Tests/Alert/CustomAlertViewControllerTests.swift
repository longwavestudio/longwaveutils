//
//  CustomAlertViewControllerTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 09/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils
class CustomAlertViewControllerTests: XCTestCase {

    func testCustomAlertControllerIsCreated() {
        let currentBundle = Bundle(for: CustomAlertViewController.self)
        let finder = AlertControllerFinder()
        let controllerId = "CustomAlertViewController"
        let sut: CustomAlertViewController?
        sut = try? DefaultStoryboardLoader.viewController(with: controllerId,
                                                          finder: finder,
                                                          bundle: currentBundle)
        XCTAssertNotNil(sut)
    }    

}

/// AlertController type error
extension CustomAlertViewControllerTests {
    
}


/// AlertControllerFinder
extension CustomAlertViewControllerTests {
    func testStoryboardIdShouldBeAlert() {
        let sut = AlertControllerFinder()
        XCTAssertEqual(try? sut.storyboardId(for: "CustomAlertViewController"), "Alert")
    }
    
    func testViewcontrollersInStoryBoardFinderShouldBeOne() {
        let sut = AlertControllerFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "Alert").count, 1)
    }
    
}
