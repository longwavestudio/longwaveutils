//
//  BackgroundForegroundObserverTests.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

class BackgroundForegroundObserverTests: XCTestCase {

    func testConvenienceInitShouldInitializeDefaultNotificationCenter() {
        let sut = DefaultBackgroundForegroundObserver()
        XCTAssertEqual(sut.notificationCenter, NotificationCenter.default)

    }
    func testWhenForegroundIsFiredShouldCallCallback() {
        let notificationCenter = NotificationCenter()
        let sut = DefaultBackgroundForegroundObserver(notificationCenter: notificationCenter)
        var didCallBackgroundObserver = false
        var didCallForegroundObserver = false
        var didCallActiveObserver = false

        sut.startObserving(onBackground: {
            didCallBackgroundObserver = true
        }, onForeground:  {
            didCallForegroundObserver = true
        }, onActive: {
            didCallActiveObserver = true
        })
        notificationCenter.post(name: UIApplication.willEnterForegroundNotification, object: nil)
        XCTAssertTrue(didCallForegroundObserver)
        XCTAssertFalse(didCallBackgroundObserver)
        XCTAssertFalse(didCallActiveObserver)
    }
    
    func testWhenBackgroundIsFiredShouldCallCallback() {
        let notificationCenter = NotificationCenter()
        let sut = DefaultBackgroundForegroundObserver(notificationCenter: notificationCenter)
        var didCallBackgroundObserver = false
        var didCallForegroundObserver = false
        var didCallActiveObserver = false

        sut.startObserving(onBackground: {
            didCallBackgroundObserver = true
        }, onForeground: {
            didCallForegroundObserver = true
        }, onActive: {
            didCallActiveObserver = true
        })
        notificationCenter.post(name: UIApplication.didEnterBackgroundNotification, object: nil)
        XCTAssertFalse(didCallForegroundObserver)
        XCTAssertTrue(didCallBackgroundObserver)
        XCTAssertFalse(didCallActiveObserver)

    }
    
    func testWhenApplicationActiveIsFiredShouldCallCallback() {
        let notificationCenter = NotificationCenter()
        let sut = DefaultBackgroundForegroundObserver(notificationCenter: notificationCenter)
        var didCallBackgroundObserver = false
        var didCallForegroundObserver = false
        var didCallActiveObserver = false

        sut.startObserving(onBackground: {
            didCallBackgroundObserver = true
        }, onForeground: {
            didCallForegroundObserver = true
        }, onActive: {
            didCallActiveObserver = true
        })
        notificationCenter.post(name: UIApplication.didBecomeActiveNotification, object: nil)
        XCTAssertFalse(didCallForegroundObserver)
        XCTAssertFalse(didCallBackgroundObserver)
        XCTAssertTrue(didCallActiveObserver)

    }
}
