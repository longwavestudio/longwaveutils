//
//  TabFlowDependencyMock.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 04/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
@testable import LongwaveUtils
final class TabFlowDependencyMock {
    var menu: MenuFlow? = MenuFlowMock()
    var flows: [TabFlowEmbeddable]
    init(flows: [TabFlowEmbeddable]) {
        self.flows = flows
    }
}

extension TabFlowDependencyMock: TabFlowDependency {}
