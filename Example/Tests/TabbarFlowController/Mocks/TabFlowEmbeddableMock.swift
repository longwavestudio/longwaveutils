//
//  TabFlowEmbeddableMock.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 04/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
@testable import LongwaveUtils

final class TabFlowEmbeddableMock {
    var onSetBadge: ((TabItemBadge) -> Void)?
    var icon: UIImage = UIImage()
    var iconSelected: UIImage = UIImage()
    var title: String = "aaa"
    
    var onEmbed: (() -> Void)?
    var onSubscribe: (() -> Void)?
    private var onCompletion: ((_ info: [String: Any]) -> Void)?
    private var onMenuEvent: ((String?, @escaping MenuFlowEventBlock) -> Void)?
    func triggerCompletion() {
        onCompletion?([:])
    }
    
    func triggerMenuEvent(block: @escaping MenuFlowEventBlock) {
        onMenuEvent?("", block)
    }
    
    func updateBadgeWith(value: String?, color: UIColor?, when: TabItemBadge.Condition = .always) {
        onSetBadge?(TabItemBadge(value: value, color: color, updateCondition: when))
    }
}
extension TabFlowEmbeddableMock: TabFlowEmbeddable {
    
    func embed(in viewController: UIViewController, onCompletion: @escaping (_ info: [String: Any]) -> Void) {
        self.onCompletion = onCompletion
        onEmbed?()
    }
    
    func subscribe(onMenuOpen: @escaping (String?, @escaping MenuFlowEventBlock) -> Void) {
        self.onMenuEvent = onMenuOpen
        onSubscribe?()
    }
}
