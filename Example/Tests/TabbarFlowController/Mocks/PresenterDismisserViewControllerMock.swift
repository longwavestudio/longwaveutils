//
//  PresenterDismisserViewControllerMock.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 04/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

class PresenterDismisserViewControllerMock: UIViewController {
    var shouldCallSuper = false
    var onEmbed: ((_ viewControllerToPresent: UIViewController) -> Void)?
    var onDisembed: (() -> Void)?

    var onPresent: ((_ viewControllerToPresent: UIViewController) -> Void)?
    var onDismiss: (() -> Void)?
    
    var transitionCompletionBlock: (() -> Void)?
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if shouldCallSuper {
            super.present(viewControllerToPresent, animated: true, completion: completion)
        }
        transitionCompletionBlock = completion
        onPresent?(viewControllerToPresent)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if shouldCallSuper {
            super.dismiss(animated: true, completion: completion)
        }
        transitionCompletionBlock = completion
        onDismiss?()
    }
    
    override func embed(viewController: UIViewController, in viewToEmbed: UIView? = nil) -> UIViewController {
        if shouldCallSuper {
            super.embed(viewController: viewController)
        }
        onEmbed?(viewController)
        return viewController
    }
    
    override func removeEmbeddedViewController(embeddedViewController: UIViewController) {
        if shouldCallSuper {
            super.removeEmbeddedViewController(embeddedViewController: embeddedViewController)
        }
        onDisembed?()
    }
    
    func triggerCompletion() {
        transitionCompletionBlock?()
    }
}
