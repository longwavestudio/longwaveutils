//
//  MenuFlowMock.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 05/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
@testable import LongwaveUtils

final class MenuFlowMock {
    var onEmbed: (() -> Void)?

    var onSetup: (() -> Void)?
    
    private var block: MenuEventBlock?
    func triggerBlock(action: String, tab: Int) {
        block?(action, tab)
    }
}

extension MenuFlowMock: MenuFlow {
    func embedMenu(in viewController: UIViewController, animated: Bool, type: String?, onClose: @escaping () -> Void) {
        onEmbed?()

    }
    
    func setup(type: String?, actionBlock block: @escaping MenuEventBlock) {
        self.block = block
        onSetup?()
    }
}
