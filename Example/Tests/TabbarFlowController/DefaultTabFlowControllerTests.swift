//
//  DefaultTabFlowControllerTests.swift
//  LongwaveUtils_Tests
//
//  Created by Alessio Bonu on 04/05/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import XCTest
@testable import LongwaveUtils

private typealias Presenter = PresenterDismisserViewControllerMock

class DefaultTabFlowControllerTests: XCTestCase {
    private var sut: DefaultTabFlowController?
    
    private func embed(dependency: TabFlowDependencyMock,
                       onEmbed: @escaping (UITabBarController) -> Void) {
        sut = DefaultTabFlowController(dependency: dependency)
        let presenter = Presenter()
        presenter.onEmbed = { vc in
            if let vc = vc as? UITabBarController {
                onEmbed(vc)
            }
        }
        sut?.embed(in: presenter) { _ in }
    }
    
    func testTabFlowControllerShouldBeEmbedded() throws {
        let dependency = TabFlowDependencyMock(flows: [TabFlowEmbeddableMock()])
        var didCallEmbed = false
        embed(dependency: dependency) { _ in
            didCallEmbed = true
        }
        XCTAssertTrue(didCallEmbed)
    }
    
    func testContainerViewControllersAreCreatedAndSetForAllTheFlowsInDependency() {
        let dependency = TabFlowDependencyMock(flows: [TabFlowEmbeddableMock(),
                                                       TabFlowEmbeddableMock(),
                                                       TabFlowEmbeddableMock()])
        let exp = expectation(description: "Check containers")
        embed(dependency: dependency) { vc in
            DispatchQueue.main.async {
                XCTAssertEqual(vc.viewControllers?.count, 3)
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 0.1)
    }
    
    func testFlowsAreEmbeddedInContainers() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let flow3 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1,
                                                       flow2,
                                                       flow3])
        let exp = expectation(description: "Check embedded flows")
        let group = DispatchGroup()
        group.enter()
        flow1.onEmbed = {
            group.leave()
        }
        group.enter()
        flow2.onEmbed = {
            group.leave()
        }
        group.enter()
        flow3.onEmbed = {
            group.leave()
        }
        group.notify(queue: DispatchQueue.main) {
            exp.fulfill()
        }
        embed(dependency: dependency) { vc in }
        
        waitForExpectations(timeout: 0.1)
    }
    
    func test_flowsShouldUpdate_BadgeValue() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let flow3 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1, flow2, flow3])
        var tabController: UITabBarController? = nil
        embed(dependency: dependency, onEmbed: { tabViewController in
            tabController = tabViewController
        })
        flow1.updateBadgeWith(value: "Test 1", color: nil)
        flow3.updateBadgeWith(value: "Badge 3", color: nil)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeValue, "Test 1")
        XCTAssertNil(tabController?.tabBar.items?[1].badgeValue)
        XCTAssertEqual(tabController?.tabBar.items?[2].badgeValue, "Badge 3")
    }
    
    func test_flowsShouldUpdate_BadgeColor() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1, flow2])
        var tabController: UITabBarController? = nil
        embed(dependency: dependency, onEmbed: { tabViewController in
            tabController = tabViewController
        })
        flow1.updateBadgeWith(value: nil, color: .green)
        XCTAssertNil(tabController?.tabBar.items?[0].badgeValue)
        XCTAssertNil(tabController?.tabBar.items?[1].badgeValue)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeColor, .green)
    }
    
    func test_flowsShouldUpdate_BadgeValueAndColor() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1, flow2])
        var tabController: UITabBarController? = nil
        embed(dependency: dependency, onEmbed: { tabViewController in
            tabController = tabViewController
        })
        flow1.updateBadgeWith(value: "Test 1", color: .red)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeValue, "Test 1")
        XCTAssertNil(tabController?.tabBar.items?[1].badgeValue)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeColor, .red)
        XCTAssertNil(tabController?.tabBar.items?[1].badgeColor)
    }
    
    func test_flowsShouldUpdate_BadgeValue_OnlyWhenNotSelected() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1, flow2])
        var tabController: UITabBarController? = nil
        embed(dependency: dependency, onEmbed: { tabViewController in
            tabController = tabViewController
        })
        tabController?.selectedIndex = 0
        flow1.updateBadgeWith(value: "Test 1", color: .blue, when: .onlyWhenUnselected)
        XCTAssertNil(tabController?.tabBar.items?[0].badgeValue)
        XCTAssertNil(tabController?.tabBar.items?[0].badgeColor)
        tabController?.selectedIndex = 1
        flow1.updateBadgeWith(value: "Test 1", color: .blue, when: .onlyWhenUnselected)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeValue, "Test 1")
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeColor, .blue)
        sut = nil
    }
    
    func test_flowsShouldUpdate_BadgeValue_Always() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1, flow2])
        var tabController: UITabBarController? = nil
        embed(dependency: dependency, onEmbed: { tabViewController in
            tabController = tabViewController
        })
        tabController?.selectedIndex = 0
        flow1.updateBadgeWith(value: "Test 1", color: .blue, when: .always)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeValue, "Test 1")
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeColor, .blue)
        tabController?.selectedIndex = 1
        flow1.updateBadgeWith(value: "Test 1", color: .blue, when: .always)
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeValue, "Test 1")
        XCTAssertEqual(tabController?.tabBar.items?[0].badgeColor, .blue)
        sut = nil
    }
    
    func testOnEmbeddedFlowCompletionShouldTriggerFlowCompletion() {
        let flow1 = TabFlowEmbeddableMock()
        let dependency = TabFlowDependencyMock(flows: [flow1])
        let sut = DefaultTabFlowController(dependency: dependency)
        var didCallCompletion = false
        sut.embed(in: UIViewController()) { _ in
             didCallCompletion = true
        }
        flow1.triggerCompletion()
        XCTAssertTrue(didCallCompletion)
    }
    
    func testOnEmbeddedFlowShouldSubscribeFlowsForMenuEventsIfMenuFlowIsSet() {
        let flow1 = TabFlowEmbeddableMock()
        var didCallSubscribe = false
        flow1.onSubscribe = {
            didCallSubscribe = true
        }
        let dependency = TabFlowDependencyMock(flows: [flow1])
        let sut = DefaultTabFlowController(dependency: dependency)
        sut.embed(in: UIViewController()) { _ in }
        flow1.triggerCompletion()
        XCTAssertTrue(didCallSubscribe)
    }
    
    func testOnEmbeddedFlowShouldNotSubscribeFlowsForMenuEventsIfMenuFlowIsNil() {
        let flow1 = TabFlowEmbeddableMock()
        var didCallSubscribe = false
        flow1.onSubscribe = {
            didCallSubscribe = true
        }
        let dependency = TabFlowDependencyMock(flows: [flow1])
        dependency.menu = nil
        let sut = DefaultTabFlowController(dependency: dependency)
        sut.embed(in: UIViewController()) { _ in }
        flow1.triggerCompletion()
        XCTAssertFalse(didCallSubscribe)
    }
    
    private func openMenuAndTriggerAction(flows: [TabFlowEmbeddableMock],
                                          tab: Int = 0,
                                          menuMock: MenuFlowMock = MenuFlowMock(),
                                          onAction: @escaping (String) -> Void) -> DefaultTabFlowController {
        flows[0].onSubscribe = {
            flows[0].triggerMenuEvent { action in
                onAction(action)
            }
            DispatchQueue.main.async {
                menuMock.triggerBlock(action: "aaa", tab: tab)
            }
        }
        let dependency = TabFlowDependencyMock(flows: flows)
        dependency.menu = menuMock
        let sut = DefaultTabFlowController(dependency: dependency)
        sut.embed(in: UIViewController()) { _ in }
        flows[0].triggerCompletion()
        return sut
    }
    
    func testOnMenuActionFlowIsInformed() {
        let flow1 = TabFlowEmbeddableMock()
        let exp = expectation(description: "testOnMenuActionFlowIsInformed")
        let  _ = openMenuAndTriggerAction(flows: [flow1]) { _ in
            exp.fulfill()
        }
        waitForExpectations(timeout: 0.1)
    }
    
    
    func testOnMenuActionShouldSelectTheRightTab() {
        let flow1 = TabFlowEmbeddableMock()
        let flow2 = TabFlowEmbeddableMock()
        var sut: DefaultTabFlowController? = nil
        let exp = expectation(description: "testOnMenuActionShouldSelectTheRightTab")
        sut = openMenuAndTriggerAction(flows: [flow1, flow2], tab: 1) { _ in }
        DispatchQueue.main.asyncAfter(deadline: . now()) {
            XCTAssertEqual(sut?.tabController?.selectedIndex, 1)
            exp.fulfill()
        }
        waitForExpectations(timeout: 0.1)
    }
    
    func testOnSubscribeShouldCallSetup() {
        let flow1 = TabFlowEmbeddableMock()
        let exp = expectation(description: "testOnSubscribeShouldCallSetup")
        let mock = MenuFlowMock()
        mock.onSetup = {
            exp.fulfill()
        }
        let _ = openMenuAndTriggerAction(flows: [flow1],
                                       tab: 0,
                                       menuMock: mock) { _ in }
        waitForExpectations(timeout: 0.1)
    }
    
    func testOnMenuOpenShouldEmbedMenuFlowController() {
        let flow1 = TabFlowEmbeddableMock()
        let exp = expectation(description: "embed menu")
        let mock = MenuFlowMock()
        mock.onEmbed = {
            exp.fulfill()
        }
        let _ = openMenuAndTriggerAction(flows: [flow1],
                                       tab: 0,
                                       menuMock: mock) { _ in }
        waitForExpectations(timeout: 0.1)
    }
}
